import os
from shutil import move
import pandas as pd
import numpy as np
import xlrd
import time
from math import isnan
import natsort
from xlsxwriter.utility import xl_rowcol_to_cell, xl_cell_to_rowcol, xl_col_to_name
from datetime import datetime, timedelta

version = '0.7.0'

mark = ["o", "^", "2", "s", "p" , "P", "*", "X", "D"]

colors = ['black', 'blue', 'brown', 'cyan',
          'gray', 'green', 'lime', 'magenta',
          'navy', 'orange', 'pink', 'purple', 'red', 'silver', 'yellow']

def all_files(ext):
    #returns list of all file names in current directory that end with ext
    sampleData = []
    files = os.listdir('.')
    for file in files:
        if file.endswith(ext):
            sampleData.append(file)
    return sampleData

def af_ext_path(path, ext):
    #returns a list of all file names in the directory (Path) matching extension
    #returned filenames do not include extension
    sampleData = []
    files = os.listdir(path)
    for file in files:
        if file.endswith(ext):
            sampleData.append(file)
    return sampleData

class Procedure:
    def __init__(self):
        self.start_time = None
        self.end_time = None
        self.execution_time = None
        self.name = None
        self.repeat = None
        self.commands = []  # here will be a list of Command objects that are within the procedure
        self.command_range = []  # here is a list of numbers that are each object of self.commands[x].command_number
        self.open_commands = []  # list of Command objects that are not closed,
        self.errors = {}

    def set_starttime(self, start_time):
        self.start_time = start_time

    def set_endtime(self, end_time):
        self.end_time = end_time
        self.calc_exectime()

    def calc_exectime(self):
        if type(self.start_time) == datetime and type(self.end_time) == datetime:
            self.execution_time = self.end_time - self.start_time

    def set_name(self, name_msg):
        name = name_msg[27:].split(",")[0][5:]
        self.name = name

    def set_repeat(self, repeat_message):
        self.repeat = repeat_message[27:].split(",")[-1][9:-1]

    def add_command(self, command_ob):
        if command_ob.execution_time == None:
            self.open_commands.append(command_ob)
        self.commands.append(command_ob)

    def set_commandrange(self):
        return_range = []
        proc_errors ={}
        for com in self.commands:
            return_range.append(com.command_number)
            proc_errors = {**proc_errors, **com.errors}
        self.command_range = return_range
        self.errors = proc_errors

    def close_procedure(self, date_time_ob, message):
        name = message[27:].split(",")[0][5:]
        repeat = message[27:].split(",")[-1][9:-1]
        if self.name == name:
            if self.repeat == repeat:
                self.set_endtime(date_time_ob)
                self.set_commandrange()
                return 0
            else:
                print("Repeat Mismatch: Procedure %s, self.repeat = %s, close repeat = %s"%(self.name, self.repeat, repeat))
                self.set_endtime(date_time_ob)
                self.set_commandrange()
                return 2
        else:
            return 1

    def proc_output_df(self, writer, workbook, temp_df):
        out_dict = {}
        #self info
        try:
            out_dict['Procedure Name'] = self.name
            out_dict['Procedure Repeats'] = int(self.repeat)
            #print(self.command_range)
            out_dict['Start Time'] = self.start_time
            out_dict['End Time'] = self.end_time
            out_dict['Execution Time'] = self.execution_time
            out_dict['Number of Commands'] = len(self.command_range)
            out_dict['Number of Errors'] = len(self.errors)
        except TypeError:
            out_dict['Procedure Name'] = self.name
            out_dict['Procedure Repeats'] = None
            out_dict['First Command'] = None
            out_dict['Final Command'] = None
            out_dict['Start Time'] = None
            out_dict['End Time'] = None
            out_dict['Execution Time'] = None
            out_dict['Number of Commands'] = len(self.commands)
            out_dict['Number of Errors'] = None
            self.name = "Default_Manual_Proc"
        except IndexError:
            out_dict['First Command'] = None
            out_dict['Final Command'] = None
        out_df = pd.DataFrame.from_dict(out_dict, orient='index', columns=['Procedure Info'])
        if len(self.name) >=29:
            tab_name = self.name[:28] +"."
        else:
            tab_name = self.name
        out_df.to_excel(writer, sheet_name=tab_name)

        time_dur_format = workbook.add_format({'num_format':'dd/mm/yy hh:mm:ss.000', 'align':'center'})
        general_format = workbook.add_format()
        general_format.set_num_format(0)
        script_overview_sheet = writer.sheets[tab_name]
        script_overview_sheet.set_column(0, 0, 25)
        script_overview_sheet.set_column(1, 1, 43)
        script_overview_sheet.set_column(2, 2, 12)
        script_overview_sheet.write_formula('B6', '=B5-B4', time_dur_format)
        #script_overview_sheet.write_formula('C6', '=B6', general_format)
        proc_dict = {}
        #proc_dict['idx'] = ['Start Time', 'End Time', "Execution Time", 'Number of Commands']
        proc_df, grp_cmd_mod_df, _z, proc_errors = self.proc_return_df(temp_df)
        cnm = len(proc_df.index)
        proc_df.to_excel(writer, sheet_name=tab_name, startcol=3, startrow=1, index = True, header=False)
        #start Group CMD modules here
        script_overview_sheet.set_column(3, 6, 15)
        script_overview_sheet.set_column(7, 8, 25)
        script_overview_sheet.set_column(10, 11, 25)
        script_overview_sheet.set_column(12, 19, 15)
        header_format = workbook.add_format({
            'bold': True,
            'text_wrap': True,
            'valign': 'top',
            'border': 1
        })
        for col_num, val in enumerate(proc_df.columns.values):  #rewrite headers in new format
            script_overview_sheet.write(0, col_num+4, val, header_format)
        #proc_errors_df = pd.DataFrame.from_dict(proc_errors, orient='index', columns=['Log Message', 'Suspected Error'])
        start_row = len(proc_df.index)+3
        proc_errors.to_excel(writer, sheet_name=tab_name, startcol=3, startrow=start_row, index=True, header=True)
        #trim tempdf
        cmd_colors = []
        for i in range(start_row-3):
            script_overview_sheet.write_formula(i+1, 10, '= I%s - H%s'%(i+2, i+2), time_dur_format)
            cmd_colors.append({'fill':{'color': colors[i%len(colors)]}})
        bar_chart = workbook.add_chart({'type': 'bar', 'subtype': 'stacked'})
        bar_chart.add_series({
            'name': [tab_name, 3, 0],
            'categories': '''=('%s'!$B$2,'%s'!$D$2:$D$%s)''' % (tab_name, tab_name, (2+cnm)),  # $N$16
            'values': '''=('%s'!$B$4,'%s'!$H$2:$H$%s)''' % (tab_name, tab_name, 2+cnm),
            'fill': {'none': True}
        })
        bar_chart.add_series({
            'name': [tab_name, 5, 0],
            'values': '''=('%s'!$B$6,'%s'!$K$2:$K$%s)''' % (tab_name, tab_name, (2+cnm)),
            'points': cmd_colors
        })
        bar_chart.set_x_axis({
            'date_axis': True,
            'min': excel_date(self.start_time),
            'num_format': 'hh:mm:ss.000'
        })
        bar_chart.set_size({
            'width': 1000,
            'height': 600
        })
        bar_chart.set_legend({'position': 'right'})
        script_overview_sheet.freeze_panes(0,7)
        if temp_df is None:
            index = []
        else:
            index = temp_df.index
        if len(index) == 0:
            script_overview_sheet.insert_chart('D15', bar_chart)
            pd.DataFrame(columns=['DateTime', 'TEC Temp', 'Sensor Temp']).to_excel(writer, sheet_name=tab_name, startcol=0, startrow=14, index=False, header=True)
        else:
            starttime_64 = np.datetime64(self.start_time)
            endtime_64 = np.datetime64(self.end_time)
            trim_start_df = temp_df.loc[temp_df['DateTime'].values > starttime_64]
            sub_df = trim_start_df.loc[trim_start_df['DateTime'].values < endtime_64]
            # put tempdf on proc tab
            sub_df.to_excel(writer, sheet_name=tab_name, startcol=0, startrow=14, index=False, header=True)
            scatter_chart = workbook.add_chart({'type': 'scatter', 'subtype': 'smooth_with_markers'})
            scatter_chart.add_series({
                'name': [tab_name, 14, 1],
                'categories': '''='%s'!$A$16:$A$%s''' % (tab_name,(15+len(sub_df.index))),
                'values': '''='%s'!$B$16:$B$%s''' % (tab_name, (15+len(sub_df.index))),
                'y2_axis': True
            })#TEC temp
            scatter_chart.add_series({
                'name': [tab_name, 14, 2],
                'categories': '''='%s'!$A$16:$A$%s''' % (tab_name, (15 + len(sub_df.index))),
                'values': '''='%s'!$C$16:$C$%s''' % (tab_name, (15 + len(sub_df.index))),
                'y2_axis': True
            })#Sensor Temp
            combine_chart = bar_chart.combine(scatter_chart)
            scatter_chart.set_y2_axis({'name':'Temperature (C)'})
            script_overview_sheet.insert_chart('D15', bar_chart)
        return writer, workbook, grp_cmd_mod_df

    def proc_return_df(self, temp_df):
        if len(self.name) >= 29:
            tab_name = self.name[:28] + "."
        else:
            tab_name = self.name
        proc_dict = {}
        # proc_dict['idx'] = ['Start Time', 'End Time', "Execution Time", 'Number of Commands']
        error_dict = {}
        error_index = 0
        for proc in self.commands:
            if len(proc.errors) == 1:
                if proc.errors['Warning'] == 0:
                    pass  #no errors or warnings
                else:
                    print("Some Warnings")
            else:
                for key in proc.errors:
                    if key is 'Warning':
                        numwarn = proc.errors[key]
                        if numwarn>0:
                            print("Some Warnings, 2err")
                    elif key == 'Register Callback':
                        date_time, returns = proc.errors[key]
                        if returns == '0x0':
                            #normal
                            pass
                        else:
                            error_dict[error_index] = ['Abnormal Register Callback', date_time, returns]
                            error_index += 1
                    else:
                        #error line type (key = datetime, value = infos)
                        error_dict[error_index] = ['Error/Warning', key, proc.errors[key]]
                        error_index += 1
            if proc.ctype == 0:  #normal cmd
                module = proc.module
                number = proc.command_number
                mod_cmnd_type = proc.module_cmnd_type
                mod_params = proc.module_params
                start_time = proc.start_time
                end_time = proc.end_time
                exec_time = proc.execution_time
                returns = proc.returns
                act_exec_time = str(proc.act_exec_time)
                proc_dict[number] = [module, mod_cmnd_type, mod_params,
                                     start_time, end_time, returns, exec_time, act_exec_time]
            else:  #Group Step
                #main cmd pull
                main_cmd = proc.main_step
                module = main_cmd.module
                number = main_cmd.command_number
                print("Group ", number)
                proc_dict_idx = "_".join([number, 'Main'])
                mod_cmnd_type = main_cmd.module_cmnd_type
                mod_params = main_cmd.module_params
                start_time = main_cmd.start_time
                end_time = main_cmd.end_time
                exec_time = main_cmd.execution_time
                returns = main_cmd.returns
                act_exec_time = str(main_cmd.act_exec_time)
                proc_dict[proc_dict_idx] = [module, mod_cmnd_type, mod_params,
                                     start_time, end_time, returns, exec_time, act_exec_time]
                asub_num = 0
                for sub_cmd in proc.sub_cmnds:
                    module = sub_cmd.module
                    number = sub_cmd.command_number
                    mod_cmnd_type = sub_cmd.module_cmnd_type
                    mod_params = sub_cmd.module_params
                    start_time = sub_cmd.start_time
                    end_time = sub_cmd.end_time
                    if not proc.atype:
                        #noasync
                        print("Sub ", number)
                        proc_dict_idx = "_".join([number, 'Sub%s'%asub_num])
                        asub_num += 1
                    else:
                        #async
                        print("aSub ", number)
                        proc_dict_idx = "_".join([number, 'aSub%s'%asub_num])
                        asub_num+=1
                    exec_time = sub_cmd.execution_time
                    returns = sub_cmd.returns
                    act_exec_time = str(sub_cmd.act_exec_time)
                    proc_dict[proc_dict_idx] = [module, mod_cmnd_type, mod_params,
                                         start_time, end_time, returns, exec_time, act_exec_time]

        proc_df_cols = ['Module', 'Module Command Type', 'Command Parameters',
                        'Start Time', 'End Time', 'Return', 'Execution Time', 'Actual Execution Time']
        proc_df = pd.DataFrame.from_dict(proc_dict, orient='index', columns=proc_df_cols)
        proc_df.index.name = 'Command Number'
        proc_return_df = proc_df.loc[:, ['Module', 'Module Command Type', 'Command Parameters', 'End Time','Return']]
        abnormal_returns = proc_return_df.loc[proc_return_df['Return'] != '0x0', :]
        if not abnormal_returns.empty:
            for cmd_idx in abnormal_returns.index.values:
                ret_mod, ret_modcmd, ret_params, ret_endtime, ret  = abnormal_returns.loc[cmd_idx]
                error_dict[error_index] = ['Abnormal Command Return', ret_endtime, cmd_idx + ' return:  '+ ret]
                error_index+=1
        proc_error_df = pd.DataFrame.from_dict(error_dict, orient='index',
                                               columns=['Error Type', 'DateTime', 'Message/Returns'])
        # start Group CMD modules here
        grp_cmd_mod_df = proc_df.loc[:, ('Module', 'Module Command Type', 'Start Time', 'End Time')]
        grp_cmd_mod_df['Procedure'] = tab_name
        # trim tempdf
        if temp_df is None:
            index = []
        else:
            index = temp_df.index
        if len(index) == 0:
            sub_df = pd.DataFrame(columns=['DateTime', 'TEC Temp', 'Sensor Temp'])
        else:
            starttime_64 = np.datetime64(self.start_time)
            endtime_64 = np.datetime64(self.end_time)
            trim_start_df = temp_df.loc[temp_df['DateTime'].values > starttime_64]
            sub_df = trim_start_df.loc[trim_start_df['DateTime'].values < endtime_64]
        proc_df = proc_df.dropna(axis=0)
        grp_cmd_mod_df = grp_cmd_mod_df.dropna(axis=0)
        sub_df = sub_df.dropna(axis=0)
        return proc_df, grp_cmd_mod_df, sub_df, proc_error_df

class Command:
    def __init__(self):
        self.ctype = 0
        self.start_time = None
        self.end_time = None
        self.execution_time = None
        self.command_number = None  #from TRACE#
        self.module = None #
        self.module_cmnd_type = None #
        self.module_params = None
        self.returns = None
        self.errors = {'Warning':0}   #from xml callback
        self.act_exec_time = None

    def set_starttime(self, start_time):
        self.start_time = start_time

    def set_endtime(self, end_time):
        self.end_time = end_time
        self.calc_exectime()

    def calc_exectime(self):
        if type(self.start_time) == datetime and type(self.end_time) == datetime:
            self.execution_time = self.end_time - self.start_time

    def set_commandnumber(self, command_num):
        if self.module is None:
            #possibly an error
            number = "_".join([str(command_num), 'None'])
        else:
            number = "_".join([str(command_num), self.module])
        self.command_number = number

    def set_module(self, mod_message):
        dot_split = mod_message.split(":")
        self.module = dot_split[1].split(",")[0][1:] #trim space
        self.set_module_cmnd_type(dot_split)
        self.set_module_params(dot_split)

    def set_module_cmnd_type(self, dot_split):
        self.module_cmnd_type = dot_split[1].split(",")[1][1:] #trim space

    def set_module_params(self, dot_split):
        if len(dot_split)>=3:
            self.module_params = dot_split[2][1:].split(".")[0] #trim space
        else:
            self.module_params = dot_split[1][1:].split(".")[0]

    def close_command(self, date_time_ob, message, read_cur_cmdnum):
        dot_split = message.split(":")
        if self.module == dot_split[1].split(",")[0][1:]:
            if self.module_cmnd_type == dot_split[1].split(",")[1][1:]:
                if self.module_params == dot_split[2][1:-9]:
                    self.returns = dot_split[-1]
                    self.set_endtime(date_time_ob)
                    self.set_commandnumber(read_cur_cmdnum+1)
                    return 0
                else:
                    print("Module Params mismatch:  Command Mod %s, Mod Type %s, StartTime %s"%(self.module, self.module_cmnd_type, self.start_time))
            else:
                return 1
        else:
            return 1

    def trace_command(self, trace_save_list):
        time_act_start = None
        time_act_fin = None
        trace_match = []
        mod_ret = None
        command_num = None
        ret = None
        for trace_tuple in trace_save_list:
            (tup_date_time, tup_message) = trace_tuple
            dot_split = tup_message.split(":")
            if dot_split[0] == "++ImageBuf push":  #unneeded save
                pass
            elif len(dot_split) == 3: #(++)
                module = dot_split[1].split(",")[0][1:] #trim space
                module_cmnd_type = dot_split[1].split(",")[1][1:] #trim space
                module_params = dot_split[2][1:].split(".")[0] #trim space, remove bSync = True
                if self.module == module:
                    if self.module_cmnd_type == module_cmnd_type:
                        if self.module_params == module_params:
                            time_act_start = tup_date_time
                            trace_match.append(2)
                        else:
                            trace_match.append(1)
                    else:
                        trace_match.append(0)
                else:
                    trace_match.append(False)
            elif len(dot_split) == 4: #(--)
                module = dot_split[1].split(",")[0][1:]  # trim space
                module_cmnd_type = dot_split[1].split(",")[1][1:]  # trim space
                module_params = dot_split[2][1:-9]  # trim space, remove " with ret"
                mod_ret = dot_split[3][:-1] #trim period "."
                if self.module == module:
                    if self.module_cmnd_type == module_cmnd_type:
                        if self.module_params == module_params:
                            time_act_fin = tup_date_time
                            trace_match.append(2)
                        else:
                            trace_match.append(1)
                    else:
                        trace_match.append(0)
                else:
                    trace_match.append(False)
            elif tup_message[:15] == "captureCallback":
                print("Error", tup_date_time, tup_message)
                if self.errors is None:
                    self.errors = {tup_date_time:(tup_message, "Capture Outside of Direct Command.  Late/Early")}
                else:
                    self.errors[tup_date_time] = (tup_message, "Capture Outside of Direct Command.  Late/Early")
            else:  #errors often, may need to exception protect and add errors
                if len(dot_split) < 7:
                    continue  #may case downline errors
                else:
                    try:
                        command_num = dot_split[4][:-4]
                        ret = dot_split[6][:-4]
                    except IndexError:
                        print(tup_date_time, tup_message)
                        raise
        if trace_match.count(2) == 2:
            self.act_exec_time = time_act_fin - time_act_start
            if ret == mod_ret:
                self.returns = mod_ret
            else:
                self.returns = (mod_ret, ret)
            return 0
        else:
            return 1

    def add_error(self, error_type, time, infos):
        if error_type == 2:#register callback return
            self.errors['Register Callback'] = (time,infos)
        elif error_type == 1:  #log line type error
            self.errors[time] = infos
        elif error_type == 3:  #log line type warning
            self.errors['Warning'] += 1

class ScriptRun:
    def __init__(self):
        self.start_time = None
        self.end_time = None
        self.execution_time = None
        #self.id = None
        #self.files = None
        #self.repeat = None
        self.returns = None
        self.process = None
        self.total_commands = None
        self.procedures = []  # here will be a list of Procedure objects that are within the script
        self.open_procedures = []
        self.script_path = None
        self.temp_start_time = None
        self.temp_record_df = None
        self.script_file = None
        self.cycle = None
        self.read_cmd_num = 0

    def set_starttime(self, start_time):
        self.start_time = start_time

    def set_endtime(self, end_time):
        print(self.cycle)
        self.end_time = end_time
        self.calc_exectime()
        endtime_64 = np.datetime64(end_time)
        starttime_64 = np.datetime64(self.start_time)
        #print(starttime_64, endtime_64)
        df = self.temp_record_df
        #print(df)
        #print(df['DateTime'].values[0],df['DateTime'].values[0] > starttime_64)
        #print(df['DateTime'])
        if df is not None:
            df = df.loc[df['DateTime'].values > starttime_64]
            trim_df = df.loc[df['DateTime'].values <= endtime_64]
            self.temp_record_df = trim_df
        else:
            print("Missing Temp Record Script")
            self.temp_record_df = pd.DataFrame(columns=['DateTime', 'TEC Temp', 'Sensor Temp'])

    def calc_exectime(self):
        if type(self.start_time) == datetime and type(self.end_time) == datetime:
            self.execution_time = self.end_time - self.start_time

    def set_from_message(self, message):
        mess_comma = message.split(",")
        self.id = mess_comma[1][4:]  #id is first, trim " id:"
        self.files = mess_comma[2][13:] #files count next, trim " files count:"
        self.set_repeat(mess_comma[4][6:])

    def set_repeat(self, repeat):
        self.repeat = repeat

    def set_returns(self, message):
        comm_message = message.split(",")
        ret = comm_message[1][5:]
        if ret == self.returns:
            pass
        else:
            self.returns = '0x1'

    def add_procedure(self, procedure_ob):
        for proc in self.procedures:
            if proc.name == procedure_ob.name:
                return 1
        self.procedures.append(procedure_ob)
        self.open_procedures.append(procedure_ob)
        return 0

    def set_process(self, process_num):
        self.process = process_num

    def set_script_path(self, comma_split_msg):
        self.script_path = comma_split_msg[1][5:]
        self.script_file = self.script_path.split("/")[-1]

    def set_script_path_active(self, active_script_path):
        self.script_path = active_script_path
        self.script_file = self.script_path.split("/")[-1]
    def trace_close(self, message):
        dot_split = message.split(":")
        try:
            c = dot_split[4][:-4]
            tc = dot_split[5][:-5]
            ret = dot_split[6][:-4]
            b_end = dot_split[-2][:-5]
            end = dot_split[-1]
        except IndexError:
            b_end = 0
            end = 'false'
            c = 99
            tc = 9
            ret = 1
        if b_end == "1" and end == "true":
            self.returns = ret
        if c == tc:
            self.total_commands = tc

    def set_temp(self, temp_record_df):
        self.temp_record_df = temp_record_df
        #print(temp_record_df)

    def set_cycle(self, cycle_int):
        self.cycle = cycle_int

    def script_output_df(self, append_rep, xml_num):
        out_dict = {}
        #self info
        #out_dict['Process ID'] = self.id
        #out_dict['Number of Files'] = int(self.files)
        out_dict['Script Cycle'] = self.cycle
        out_dict['Script Path'] = self.script_path
        #out_dict['Total Commands'] = int(self.total_commands)
        out_dict['Start Time'] = self.start_time
        out_dict['End Time'] = self.end_time
        out_dict['Execution Time'] = self.execution_time
        out_dict['Returns'] = self.returns
        out_dict['Number of Procedures'] = len(self.procedures)
        #out_dict['Tempurate Record'] = self.temp_record
        #out_dict['Temp Record Start Time'] = self.temp_start_time
        out_df = pd.DataFrame.from_dict(out_dict, orient='index', columns=['Script Info'])
        cycle_type = "_".join(self.script_path.split("/")[-1][:-4].split(" "))
        fname = "XML_%s_RunScriptAnalysis_Cycle_%s_%s.xlsx"%(xml_num, append_rep, cycle_type)
        writer = pd.ExcelWriter(fname,
                                engine='xlsxwriter',
                                datetime_format='dd/mm/yy hh:mm:ss.000',
                                )
        out_df.to_excel(writer, sheet_name="Cycle Overview")
        workbook = writer.book
        time_dur_format = workbook.add_format({'num_format':'dd/mm/yy hh:mm:ss.000'})
        general_format = workbook.add_format()
        general_format.set_num_format(0)
        script_overview_sheet = writer.sheets["Cycle Overview"]
        script_overview_sheet.set_column(0, 0, 25)
        script_overview_sheet.set_column(1, 1, 43)
        script_overview_sheet.write_formula('B6', '=B5-B4', time_dur_format)
        #script_overview_sheet.write_formula('C7', '=B7', general_format)
        proc_dict = {}
        proc_dict['idx'] = ['Start Time', 'End Time', "Execution Time", 'Number of Commands', 'Number of Errors']
        try:
            self.temp_record_df.to_excel(writer, sheet_name="Cycle Temp Data")
            #print(self.temp_record_df)
            temp_record_rows = len(self.temp_record_df.index)
        except AttributeError:
            print("Reset Script Tempdf")
            pd.DataFrame(columns=['DateTime', 'TEC Temp', 'Sensor Temp']).to_excel(writer, sheet_name="Cycle Temp Data")
            temp_record_rows = 0
        temp_sheet = writer.sheets['Cycle Temp Data']
        temp_sheet.set_column(1,1, 25)
        pc = 0
        names = []
        for proc in self.procedures:
            # check repeat proc names!
            if proc.name in names:
                rep = 1
                while proc.name in names:
                    if len(proc.name) >= 31:
                        proc.name = proc.name[:28] + "_%s" % rep
                        rep += 1
                    elif len(proc.name.split("_"))>1:
                        if proc.name.split("_")[1] == 'Init':
                            proc.name = "_".join(proc.name.split("_")[0:2]) + "_%s" % rep
                        else:
                            proc.name = proc.name.split("_")[0] + "_%s"%rep
                        rep += 1
                    else:
                        proc.name = proc.name + "_%s" % rep
                        rep += 1
                names.append(proc.name)
            else:
                names.append(proc.name)
            #print(proc.name, proc.open_commands)
            name = proc.name
            start_time = proc.start_time
            end_time = proc.end_time
            exec_time = proc.execution_time
            command_rng = proc.command_range
            num_errors = len(proc.errors)
            proc_dict[name] = [start_time, end_time, exec_time, len(command_rng), num_errors]
            pc+=1
        proc_df = pd.DataFrame.from_dict(proc_dict, orient='columns')
        proc_df.to_excel(writer, sheet_name="Cycle Overview", startcol=4, startrow=0, index = False)
        start_x = 3
        start_y = 5
        proc_zero_st = None
        proc_zero_exectime = None
        proc_colors = []
        second = 'D2'
        cell = 'H2'
        for i in range(pc):
            cell = xl_rowcol_to_cell(start_x, start_y+i, row_abs=True, col_abs=True)
            first = xl_rowcol_to_cell(start_x-1, start_y+i, row_abs=True, col_abs=True)
            second = xl_rowcol_to_cell(start_x-2, start_y+i, row_abs=True, col_abs=True)
            script_overview_sheet.write_formula(cell, '=%s-%s'%(first, second), time_dur_format)
            proc_colors.append({'fill':{'color': colors[i%len(colors)]}})
            if i == 0:
                proc_zero_exectime = cell
                proc_zero_st = second
        script_overview_sheet.set_column(4, 4+pc, 20)
        final_cat_cell = xl_cell_to_rowcol(second)
        new_final_cat_cell = xl_rowcol_to_cell(final_cat_cell[0]-1, final_cat_cell[1], row_abs=True, col_abs=True)
        bar_chart = workbook.add_chart({'type': 'bar', 'subtype': 'stacked'})
        bar_chart.add_series({
            'name': ["Cycle Overview", 3, 0],
            'categories': "=('Cycle Overview'!$B$1,'Cycle Overview'!$F$1:%s)"%(new_final_cat_cell),#$N$16
            'values': "=('Cycle Overview'!$B$4,'Cycle Overview'!%s:%s)"%(proc_zero_st, second),
            'fill': {'none':True}
        })
        bar_chart.add_series({
            'name': ["Cycle Overview", 5, 0],
            'values': "=('Cycle Overview'!$B$6,'Cycle Overview'!%s:%s)" % (proc_zero_exectime, cell),
            'points':  proc_colors
        })
        bar_chart.set_x_axis({
            'date_axis': True,
            'min': excel_date(self.start_time),
            'num_format':'hh:mm:ss.000'
        })
        bar_chart.set_size({
            'width': 1300,
            'height': 600
        })
        bar_chart.set_legend({'none': True})
        scatter_chart = workbook.add_chart({'type':'scatter', 'subtype':'smooth_with_markers'})
        scatter_chart.add_series({
            'name': ["Cycle Temp Data", 0, 2],
            'categories': "='Cycle Temp Data'!$B$2:$B$%s"%(temp_record_rows+2),
            'values': "='Cycle Temp Data'!$C$2:$C$%s"%(temp_record_rows+2),
            'y2_axis': True
        })
        scatter_chart.add_series({
            'name': ["Cycle Temp Data", 0, 3],
            'categories': "='Cycle Temp Data'!$B$2:$B$%s" % (temp_record_rows + 2),
            'values': "='Cycle Temp Data'!$D$2:$D$%s" % (temp_record_rows + 2),
            'y2_axis': True
        })
        combine_chart = bar_chart.combine(scatter_chart)
        scatter_chart.set_y2_axis({'name': 'Temperature (C)'})
        bar_chart.set_legend({'position':'right'})
        script_overview_sheet.insert_chart('A15', bar_chart)

        return writer, workbook, fname

    def script_return_df(self, out_cycle_num):
        cycle_type = "_".join(self.script_path.split("/")[-1][:-4].split(" "))
        proc_dict = {}
        proc_dict['idx'] = ['Start Time', 'End Time', "Execution Time", 'Number of Commands', 'Number of Errors']
        pc = 0
        names = []
        for proc in self.procedures:
            # check repeat proc names!
            if proc.name in names:
                rep = 1
                while proc.name in names:
                    if len(proc.name) >= 31:  #for names too long for excel
                        proc.name = proc.name[:28] + "_%s" % rep
                        rep += 1
                    elif len(proc.name.split("_")) > 1:  #for names with underscore (2 case)
                        if proc.name.split("_")[1] == 'Init':#Case 0:  Device_Init (repeat proc)
                            proc.name = "_".join(proc.name.split("_")[0:2]) + "_%s" % rep
                        else:  #repeat procedure increase (Cleavage_1 already in names, add Cleavage_2)
                            proc.name = proc.name.split("_")[0] + "_%s" % rep
                        rep += 1
                    else:  #repeat procedure add _1
                        proc.name = proc.name + "_%s" % rep
                        rep += 1
                names.append(proc.name)
            else:
                if proc.name is None:
                    print("Procedure with No Name? Proc Count:  %s"%pc)
                    break
                #elif len(proc.name) >= 29:  Likely unnecessary
                    #proc.name = proc.name[:28] + "."
                else:
                    names.append(proc.name)
            # print(proc.name, proc.open_commands)
            name = proc.name
            start_time = proc.start_time
            end_time = proc.end_time
            exec_time = proc.execution_time
            command_rng = proc.command_range
            num_errors = len(proc.errors)
            proc_dict[name] = [start_time, end_time, exec_time, len(command_rng), num_errors]
            pc += 1
        proc_df = pd.DataFrame.from_dict(proc_dict, orient='columns')
        proc_df = proc_df.set_index('idx')
        proc_df = proc_df.T
        try:
            proc_df['Execution Time\n(HH:MM:SS:sss)'] = proc_df['End Time'] - proc_df['Start Time']
        except TypeError:
            proc_df['Execution Time\n(HH:MM:SS:sss)'] = [None for i in range(len(proc_df.index))]
        overview_proc_df = proc_df.drop(['Start Time', 'End Time'], axis=1)
        overview_proc_df['Procedure'] = overview_proc_df.index
        overview_proc_df = overview_proc_df.reset_index(drop=True)
        overview_proc_df['Cycle\n(Number)'] = out_cycle_num
        return cycle_type, overview_proc_df

class GroupStep:
    def __init__(self):
        self.ctype = 1
        self.start_time = None
        self.end_time = None
        self.execution_time = None
        self.command_number = None  #from TRACE#
        self.returns = None
        self.errors = {}  # from xml callback
        self.main_step = None
        self.sub_cmnds = []
        self.atype = False

    def set_starttime(self, start_time):
        self.start_time = start_time

    def set_endtime(self, end_time):
        self.end_time = end_time
        self.calc_exectime()

    def calc_exectime(self):
        if type(self.start_time) == datetime and type(self.end_time) == datetime:
            self.execution_time = self.end_time - self.start_time

    def set_commandnumber(self, command_num):
        self.command_number = command_num
        self.main_step.set_commandnumber(command_num)
        for cmd in self.sub_cmnds:
            cmd.set_commandnumber(command_num)

    def close_command(self, date_time_ob, message, read_cur_cmdnum):
        #print("Force Close GroupStep!")
        main_step_errors = self.main_step.errors
        self.set_commandnumber(read_cur_cmdnum+1)
        if self.main_step.returns == None:
            self.main_step.returns = message.split(":")[-1]
        for cmd in self.sub_cmnds:
            if cmd.returns == None:
                cmd.returns = message.split(":")[-1]
        #force end grp cmd duration
        if self.main_step.end_time == None:
            self.main_step.set_endtime(date_time_ob)
        for cmd in self.sub_cmnds:
            if cmd.end_time == None:
                cmd.set_endtime(date_time_ob)
        self.errors = {**self.errors, **main_step_errors}
        for cmd in self.sub_cmnds:
            sub_errors = cmd.errors
            self.errors = {**self.errors, **sub_errors}
        return 0

    old = """def trace_command(self, trace_save_list):
        trace_match = []
        mod_ret = None
        command_num = None
        ret = None
        for trace_tuple in trace_save_list:
            (tup_date_time, tup_message) = trace_tuple
            #print(tup_message[:21])
            dot_split = tup_message.split(":")
            if dot_split[0] == "++ImageBuf push":  #unneeded save
                pass
            elif tup_message[:15] == "captureCallback":
                if self.main_step.module == "CMOS":
                    if self.main_step.act_exec_time == None:
                        self.main_step.act_exec_time = [tup_date_time]
                    elif type(self.main_step.act_exec_time) == list:
                        self.main_step.act_exec_time.append(tup_date_time)
                    else:
                        print("Unexpected CMOS Callback Sequence.  Cannot save imagetimes, mainstep")
                else:
                    for cmd in self.sub_cmnds:
                        if cmd.module == "CMOS":
                            if cmd.act_exec_time == None:
                                cmd.act_exec_time = [tup_date_time]
                            elif type(cmd.act_exec_time) == list:
                                cmd.act_exec_time.append(tup_date_time)
                            else:
                                print("Unexpected CMOS Callback Sequence.  Cannot save imagetimes, substep")
                        else:
                            continue #not CMOS module
                            #actual cmos take image
            elif len(dot_split) == 3:  # (++)
                module = dot_split[1].split(",")[0][1:]  # trim space
                module_cmnd_type = dot_split[1].split(",")[1][1:]  # trim space
                module_params = dot_split[2][1:].split(".")[0]  # trim space, remove bSync = True
                if module == 'CMOS':  #
                    if self.main_step.module == 'CMOS':
                        continue  #info already set
                    else:   #not main step, CMOS set info
                        new_cmd = Command()
                        new_cmd.start_time = tup_date_time
                        new_cmd.set_module(tup_message)
                        self.sub_cmnds.append(new_cmd)
                elif self.main_step.module == module:  #main step NOT CMOS, already set
                    self.main_step.set_starttime(tup_date_time)
                else:  #not Mainstep, make new subcmd, save params
                    new_cmd = Command()
                    new_cmd.start_time = tup_date_time
                    new_cmd.set_module(tup_message)
                    self.sub_cmnds.append(new_cmd)
            elif len(dot_split) == 4:  # (--)
                module = dot_split[1].split(",")[0][1:]  # trim space
                module_cmnd_type = dot_split[1].split(",")[1][1:]  # trim space
                module_params = dot_split[2][1:-9]  # trim space, remove " with ret"
                mod_ret = dot_split[3][:-1]  # trim period "."
                if module == 'CMOS':  #cmos return, no useful info to save
                    continue
                if self.main_step.module == module:  #mainstep module match, NOT CMOS
                    if self.main_step.module_cmnd_type == module_cmnd_type:
                        if self.main_step.module_params == module_params:
                            self.main_step.returns = mod_ret
                            self.main_step.set_endtime(tup_date_time)
                        else:
                            trace_match.append(1)
                    else:
                        trace_match.append(0)
                else:
                    for cmd in self.sub_cmnds:
                        if cmd.module == module:
                            if cmd.module_cmnd_type == module_cmnd_type:
                                if cmd.module_params == module_params:
                                    cmd.returns = mod_ret
                                    cmd.set_endtime(tup_date_time)
            else:
                if tup_message[:20] == "--Execute GroupStep,":
                    self.set_endtime(tup_date_time)
                elif tup_message[:20] == "++Execute GroupStep,":
                    continue
                else:
                    #print("Setting Group CMD #")
                    command_num = dot_split[4][:-4]
                    ret = dot_split[6][:-4]
        self.returns = ret
        self.command_number = command_num
        return 0"""
    def trace_command(self, trace_save_list):
        command_num = None
        ret = None
        grp_main_mod = self.main_step.module
        grp_main_mod_cmdt = self.main_step.module_cmnd_type
        grp_main_params = self.main_step.module_params
        num_subs = None
        sub_commands = []
        funcstep = False #set to true when in FuncStep
        cmos_cmd = None #when cmos cmd is correctly traced, set to true, this var set to CMD()
        ignore = ["++ImageBuf push"]
        for trace_tuple in trace_save_list:
            (tup_date_time, tup_message) = trace_tuple
            dot_split = tup_message.split(":")
            #print(trace_tuple)
            if dot_split[0] in ignore:  # unneeded saves
                pass
            elif tup_message[:36] == "++Execute GroupStep, sub-cmd counts=":
                 self.set_starttime(tup_date_time)
                 if grp_main_mod == 'FuncStep':
                     self.main_step.set_starttime(tup_date_time)
            elif tup_message[:18] == "-registerCallback:":
                reg_calbk_ret = dot_split[-1]
                if cmos_cmd is not None:
                    cmos_cmd.add_error(2,tup_date_time, reg_calbk_ret)
            elif tup_message[:18] == 'DoFuncExecuteAsync':  #start helpful, finish can be delayed
                if tup_message.split(" ")[-1] == "Start.":
                    if grp_main_mod == 'FuncStep': #disable funcstep because timing is unpredictable
                        funcstep = False
                    else:
                        funcstep = True
                        self.atype = True
                else:
                    #print("end funcstep")
                    self.sub_cmnds = sub_commands
            elif tup_message[:18] == "Snapshot saveImage":
                pass
            elif len(dot_split) == 3: #(++)
                module = dot_split[1].split(",")[0][1:]  # trim space
                module_cmnd_type = dot_split[1].split(",")[1][1:]  # trim space
                module_params = dot_split[2][1:].split(".")[0]  # trim space, remove bSync = True
                if (module == grp_main_mod) and (module_cmnd_type == grp_main_mod_cmdt) and (module_params == grp_main_params):
                    self.main_step.start_time = tup_date_time
                    if grp_main_mod == 'CMOS':
                        cmos_cmd=self.main_step
                else:#desccribing new cmd (funcstep or not)
                    new_command = Command()
                    new_command.set_module(tup_message)
                    if funcstep:#Confirmed inside funcstep
                        new_command.set_starttime(tup_date_time)
                    else:#don't mess with subcmd start time, set as same as main if not funcstep
                        new_command.set_starttime(self.main_step.start_time)
                    if new_command.module == 'CMOS':
                        cmos_cmd=new_command
                    sub_commands.append(new_command)
            elif len(dot_split) == 4:  #(--)
                module = dot_split[1].split(",")[0][1:]  # trim space
                module_cmnd_type = dot_split[1].split(",")[1][1:]  # trim space
                module_params = dot_split[2][1:-9]  # trim space, remove " with ret"
                mod_ret = dot_split[3][:-1]  # trim period "."
                if funcstep:  # Confirmed inside funcstep
                    for cmd in sub_commands:
                        if (module == cmd.module) and (module_cmnd_type == cmd.module_cmnd_type) and (module_params == cmd.module_params):
                            cmd.returns = mod_ret
                            cmd.set_endtime(tup_date_time)
                            cmd.act_exec_time = cmd.execution_time
                        else:
                            #module params no match
                            pass
                else:
                    # don't mess with subcmd end time, set as same as main if not funcstep
                    for cmd in sub_commands:
                        if (module == cmd.module) and (module_cmnd_type == cmd.module_cmnd_type) and (module_params == cmd.module_params):
                            cmd.returns = mod_ret
            elif tup_message[:36] == "--Execute GroupStep, sub-cmd counts=":
                #group act exec time finish
                print("group subcmds end")
                returns = dot_split[-1][:-1]
                if funcstep:
                    self.end_time = tup_date_time
                    self.execution_time = self.end_time - self.start_time
                    self.main_step.set_endtime(tup_date_time)
                    self.main_step.act_exec_time = self.main_step.execution_time
                    self.main_step.returns = returns
                    for cmd in sub_commands:
                        if cmd.end_time == None:
                            cmd.set_endtime(tup_date_time)
                        cmd.returns = returns
                    self.sub_cmnds = sub_commands
                else:
                    self.end_time = tup_date_time
                    self.execution_time = self.end_time - self.start_time
                    self.main_step.set_endtime(tup_date_time)
                    self.main_step.act_exec_time = self.main_step.execution_time
                    self.main_step.returns = returns
                    for cmd in sub_commands:
                        if cmd.end_time == None:
                            cmd.set_endtime(tup_date_time)
                    self.sub_cmnds = sub_commands
            elif tup_message[0] == 'f':
                #final tuple
                com_split = tup_message.split(",")
                script = com_split[0]
                run = com_split[1][1:]
                total_run = com_split[2][1:]
                command = com_split[3][1:]
                returns = com_split[5][1:]
                registered_cyc = com_split[8][1:]
                self.returns = returns
            else:
                #shouldn't get to here
                print("Unknown Trace Message Capture:\n%s"%tup_message)

class ExecuteRun:
    def __init__(self):
        self.temp_record = None
        self.scripts = []
        self.process = None
        self.start_time = None
        self.end_time = None
        self.temp_record_df = None
        self.temp_start_time = None
        self.script_dict = {}
        self.script_repeat = None
        self.errors = {'Warning':0}   #from xml callback

    def set_starttime(self, start_time):
        self.start_time = start_time

    def set_endtime(self, end_time):
        self.end_time = end_time
        self.calc_exectime()
        endtime_64 = np.datetime64(end_time)
        starttime_64 = np.datetime64(self.start_time)
        # print(starttime_64, endtime_64)
        df = self.temp_record_df
        # print(df['DateTime'].values[0],df['DateTime'].values[0] > starttime_64)
        # print(df['DateTime'])
        if df is None:
            print("Missing Temp Record XML Run")
            self.temp_record_df = pd.DataFrame()
        elif df.empty:
            print("Missing Temp Record XML Run")
            self.temp_record_df = pd.DataFrame()
        else:
            df = df.loc[df['DateTime'].values > starttime_64]
            trim_df = df.loc[df['DateTime'].values <= endtime_64]
            self.temp_record_df = trim_df

    def calc_exectime(self):
        if type(self.start_time) == datetime and type(self.end_time) == datetime:
            self.execution_time = self.end_time - self.start_time

    def set_from_message(self, message):
        mess_comma = message.split(",")
        self.id = mess_comma[1][4:]  # id is first, trim " id:"
        try:
            self.files = mess_comma[2][13:]  # files count next, trim " files count:"
        except IndexError:
            self.files = 'Install/Eject Flow, Manual'
        #self.set_repeat(mess_comma[4][6:])

    def set_process(self, process_num):
        self.process = process_num

    def add_script(self, scriptob, script_order_num=None):
        if script_order_num == 0:
            self.scripts.append(0)
            self.script_dict[0] = (scriptob.script_file, scriptob)
        else:
            prev_script_order = self.scripts[-1]
            scriptob.set_cycle(prev_script_order+1)
            self.scripts.append(prev_script_order+1)
            self.script_dict[prev_script_order+1] = (scriptob.script_file, scriptob)

    def set_temp(self, temp_time, message, path):
        df = None
        self.temp_start_time = temp_time
        self.temp_record = message.split(":")[1].split("/")[-1]
        #attempt to find/import temp_record
        temp_record_suffix = "."+self.temp_record.split(".")[-1]
        similar_files = af_ext_path(path, temp_record_suffix)
        if self.temp_record_df is not None:
            print("XML Temp already Set or Manual")
        else:
            if self.temp_record in similar_files:
                df = pd.read_csv(path + "\\" + self.temp_record, names=['DateTime', 'TEC Temp', 'Sensor Temp'])
                df['DateTime'] = df['DateTime'].apply(lambda x: datetime.strptime(x, '%Y_%m_%d_%H_%M_%S_%f'))
                self.temp_record_df = df
            if df is None:
                print("Cannot Auto Set Temp, file not present in Log directory")

    def add_error(self, error_type, time, infos):
        cur_cycle = self.scripts[-1]
        script_num, cur_scriptob = self.script_dict[list(self.script_dict.keys())[-1]]
        cur_procob = cur_scriptob.procedures[-1]
        cur_procname = cur_procob.name
        if error_type == 2:#register callback return not added currently
            self.errors['Register Callback'] = (time,infos)
        elif error_type == 1:  #log line type error
            self.errors[time] = (infos, cur_cycle, cur_procname)
        elif error_type == 3:  #log line type warning
            self.errors['Warning'] += 1
            self.errors[time] = (infos, cur_cycle, cur_procname)

    def check_errors(self):
        error_dict = {}
        error_index = 0
        if len(self.errors) == 1:
            if self.errors['Warning'] == 0:
                pass  # no errors or warnings
            else:
                print("Some Warnings")
        else:
            for key in self.errors:
                if key is 'Warning':
                    numwarn = self.errors[key]
                    if numwarn > 0:
                        print("Some Warnings, 2err")
                elif key is 'Register Callback':  #depreciated
                    date_time, returns = self.errors[key]
                    if returns == '0x0':
                        # normal
                        pass
                    else:
                        error_dict[error_index] = ['Abnormal Register Callback', date_time, returns]
                        error_index += 1
                else:
                    # error line type (key = datetime, value = infos)
                    infos, cycle, proc = self.errors[key]
                    error_dict[error_index] = ['Error/Warning', key, infos, cycle, proc]
                    error_index += 1
        xml_error_df = pd.DataFrame.from_dict(error_dict, orient='index',
                                               columns=['Error Type', 'DateTime', 'Message/Returns', 'Cycle', 'Procedure'])
        return xml_error_df


def excel_date(date1):
    temp = datetime(1899, 12, 30)    # Note, not 31st Dec but 30th!
    delta = date1 - temp
    return float(delta.days) + (float(delta.seconds) / 86400)

def analyze_logfile(log, manual_temp_data):
    log_op_for = open(log, 'r')
    log_path = '\\'.join(log.split("\\")[:-1])
    print("Manual Data: ", manual_temp_data)
    print(type(manual_temp_data))
    i = 0
    sums = 0
    message_chars = []
    active_scripts = None
    trace_save = []
    xml_runs = []
    active_xml = None
    active_procedure = None
    active_command = None
    default_start_time = datetime.today()
    pause_save = []

    default_manual_command = Command()
    default_manual_command.set_starttime(default_start_time)
    default_manual_command.set_module("+Execute: Default Command, Init: Init")
    active_command = default_manual_command

    default_manual_proc = Procedure()
    default_manual_proc.set_starttime(default_start_time)
    default_manual_proc.set_name("+Execute ProcedureCommand: name=DefaultManualProc, Init.")
    active_procedure = default_manual_proc

    default_manual_script = ScriptRun()
    default_manual_script.set_starttime(default_start_time)
    default_manual_script.add_procedure(default_manual_proc)
    default_manual_script.set_script_path_active("/Init/Manual_Default_Script.xml")
    active_scripts = default_manual_script


    default_manual_xml = ExecuteRun()
    default_manual_xml.set_starttime(default_start_time)
    default_manual_xml.add_script(default_manual_script, script_order_num=0)
    active_xml = default_manual_xml

    if manual_temp_data is not None:
        manual_temp_df_comb = pd.DataFrame()
        for item in manual_temp_data:
            if item[-4:] == '.csv':
                try:
                    df = pd.read_csv(item, names=['DateTime', 'TEC Temp', 'Sensor Temp'])
                    manual_temp_df_comb = pd.concat([manual_temp_df_comb, df], axis=0)
                except Exception as e:
                    print("Could not read .csv @ %s\nException:   %s"%(item, e))
        manual_temp_df_comb['DateTime'] = manual_temp_df_comb['DateTime'].apply(lambda x: datetime.strptime(x, '%Y_%m_%d_%H_%M_%S_%f'))
        print(manual_temp_df_comb)
        print("Manual Set Temp, xml, script")
        active_xml.temp_record_df = manual_temp_df_comb
        active_scripts.set_temp(active_xml.temp_record_df)
    for line in log_op_for.readlines():
        #print(i)
        ret_qual, date_time, log_level, process, message = simple_parse_line(line)
        if ret_qual == 1:  # empty simple parse
            pass
        else:
            date_time_ob = datetime.fromisoformat(date_time)  #pull date/time
            entry_type = classify_loglevel(log_level)
            if entry_type == 0:  #INFO
                #check message[0]
                ignore = ['CX3Uninit', 'Motor init', 'CX3Init', 'finish reset!', 'start install', 'finish CMOS init',
                          'Cmos Debug Capture', 'Go:actionId', 'tecon run']
                check = message[0]
                if check == '+':  #add procedure/script
                    if message[1:18] == "execute XML Files":#new XML RUn
                        print("NewXML")
                        trace_save = []
                        new_xml_run = ExecuteRun()
                        new_xml_run.set_starttime(date_time_ob)
                        new_xml_run.set_from_message(message)
                        new_xml_run.set_process(process)
                        xml_runs.append(new_xml_run)
                        if manual_temp_data is not None:
                            new_xml_run.temp_record_df = manual_temp_df_comb
                        if active_xml.start_time == default_start_time:  #active is default
                            active_xml = new_xml_run
                        else:
                            print("Multiple active XML runs")
                            active_xml = new_xml_run #this may cause errors if old activexml is never closed
                    elif message[1:8] == 'Execute':
                        #check for procedure or command
                        if message[9:26] == "ProcedureCommand:":
                            #add procedure
                            #print("Procedure Open")
                            print("New Procedure")
                            new_procedure = Procedure()
                            new_procedure.set_starttime(date_time_ob)
                            new_procedure.set_name(message)
                            new_procedure.set_repeat(message)
                            try_new_proc = active_scripts.add_procedure(new_procedure)
                            if try_new_proc == 1:
                                #new script, script repeat
                                print("Repeat Script, Proc Name Repeat")
                                active_scripts.set_endtime(date_time_ob)
                                new_script = ScriptRun()
                                new_script.set_starttime(date_time_ob)
                                # new_script.set_from_message(message)
                                new_script.set_process(process)
                                new_script.script_file = active_scripts.script_file
                                new_script.script_path = active_scripts.script_path
                                new_script.set_temp(active_xml.temp_record_df)
                                active_xml.add_script(new_script)
                                active_scripts = new_script
                                try_new_proc = active_scripts.add_procedure(new_procedure)
                            if active_procedure.name == "DefaultManualProc":
                                active_procedure = new_procedure
                            else:
                                print("Multiple Open Procedures!!!\t%s"%active_procedure.name)
                                print(active_procedure, active_procedure.name)
                                active_procedure = new_procedure #this may cause error if old acive_proc is never closed
                        elif message[10:19] == "GroupStep":
                            print("GroupStep Open %s"%i)
                            new_groupstep = GroupStep()
                            new_groupstep.set_starttime(date_time_ob)
                            grp_step_main = Command()
                            grp_step_main.set_module(message[21:-6])
                            new_groupstep.main_step = grp_step_main
                            active_procedure.add_command(new_groupstep)
                            if active_command.module == "Default Command":
                                active_command = new_groupstep
                            else:
                                print("Multiple Active 1+ Commands inside Group(normally 2+ for subs)!")
                                active_command = new_groupstep  # this may cause error if old active_command is never closed
                        else:
                            #add command
                            print("Command Open")
                            new_command = Command()
                            new_command.set_starttime(date_time_ob)
                            new_command.set_module(message)
                            active_procedure.add_command(new_command)
                            if active_command.module == "Default Command":
                                active_command = new_command
                            else:
                                print("Multiple Active Commands Outside of Group!")
                                active_command = new_command  #this may cause error if old active_command is never closed
                        #+commands
                elif check == "-": #close procedure/script
                    if message[1:18] == "execute XML Files":   #closing active XML from completed run
                        try:
                            print("Try close xml run, line %s"%i)
                            active_scripts.set_endtime(date_time_ob)
                            #default_manual_script = ScriptRun()
                            #default_manual_script.set_starttime(default_start_time)
                            active_scripts = default_manual_script
                            active_xml.set_endtime(date_time_ob)
                            active_xml = default_manual_xml
                        except AttributeError:
                            print(default_manual_xml.start_time, default_manual_xml.end_time)
                        #active_xml set to None below
                        #active_scripts.set_returns(message)
                    elif message[1:8] == 'Execute':  #closing proc or cmd
                        # check for procedure or command
                        if message[9:26] == "ProcedureCommand:":
                            # close procedure
                            print("Procedure Close")
                            try_ret = active_procedure.close_procedure(date_time_ob, message)
                            if try_ret == 0:
                                active_scripts.open_procedures.remove(active_procedure)
                                active_procedure = default_manual_proc
                                print("proc closed and inactive")
                            else:print("Cannot close active procedure? Line: %s"%(i+1))
                        elif message[10:19] == "GroupStep":
                            print("GroupStep Close")
                            try_close_cmd = active_command.close_command(date_time_ob, message, active_scripts.read_cmd_num)
                            if active_command in active_procedure.open_commands:
                                active_procedure.open_commands.remove(active_command)
                            else:
                                print("Active command not in active_procedure")
                            if try_close_cmd == 0:
                                active_scripts.read_cmd_num += 1
                                active_command = default_manual_command
                                print("command closed and inactive1")
                            else:
                                print("Cannot close active command, line %s" % i)
                        else:
                            # add command
                            print("Command Close")
                            try_close_cmd = active_command.close_command(date_time_ob, message, active_scripts.read_cmd_num)
                            if active_command in active_procedure.open_commands: active_procedure.open_commands.remove(active_command)
                            else: print("Active command not in active_procedure")
                            if try_close_cmd == 0:
                                active_scripts.read_cmd_num +=1
                                active_command = default_manual_command
                                print("command closed and inactive1")
                            else:
                                print("Cannot close active command, line %s"%i)
                elif check == 'e':
                    comm_split = message.split(",")
                    if comm_split[0] == "execute": #execute, xml: new script
                        if active_scripts.start_time == default_start_time:
                            print("active script is Default, execute start new Script")
                            new_script = ScriptRun()
                            new_script.set_starttime(date_time_ob)
                            #new_script.set_from_message(message)
                            new_script.set_process(process)
                            new_script.set_script_path(comm_split)
                            new_script.set_temp(active_xml.temp_record_df)
                            new_script.set_cycle(0)
                            if active_xml is default_manual_xml:
                                new_xml_run = ExecuteRun()
                                new_xml_run.set_starttime(date_time_ob)
                                new_xml_run.set_from_message("+execute XML Files, id:2832, files count:5, autopause:0, loop:1, folder:script")
                                new_xml_run.set_process(process)
                                xml_runs.append(new_xml_run)
                                if manual_temp_data is not None:
                                    new_xml_run.temp_record_df = manual_temp_df_comb
                                if active_xml.start_time == default_start_time:  # active is default
                                    active_xml = new_xml_run
                            active_xml.add_script(new_script, script_order_num=0)
                            active_scripts = new_script
                        else: #switching scripts
                            print("switching scripts")
                            active_scripts.set_endtime(date_time_ob)
                            new_script = ScriptRun()
                            new_script.set_starttime(date_time_ob)
                            #new_script.set_from_message(message)
                            new_script.set_process(process)
                            new_script.set_script_path(comm_split)
                            new_script.set_temp(active_xml.temp_record_df)
                            active_xml.add_script(new_script)
                            active_scripts = new_script
                # execute xml callback: checks for errors and end after each command.  Only pulling script Path
                elif check == 'S': #Snapshot::exit  closing script procedure
                    pass
                elif check == 'f': #Script finished, passing process and update scene/state
                    if message == "finish Script Running":
                        #closed_scripts.append(active_scripts)
                        #active_scripts = None
                        #active_xml.set_endtime(date_time_ob)
                        active_xml = default_manual_xml
                    else:
                        if 'finish' in message:
                            pass #finish debug cmd
                        else:
                            print("Unknown 'f' INFO string found.  Cannot close Active Script.\n%s"%line)
                # Closing Active Script
                elif check == 'o': #onclick user input
                    pass
                elif check == 'P':
                    if message[:13] == 'PauseOrResume':
                        print("pause caught")
                        pause_save.append(active_xml)
                        pause_save.append(active_scripts)
                        pause_save.append(active_procedure)
                        pause_save.append(active_command)
                        active_xml.set_endtime(date_time_ob)
                        active_command = default_manual_command
                        active_procedure = default_manual_proc
                        active_scripts = default_manual_script
                        active_xml = default_manual_xml
                else:
                    print_message = "New Message[0] check? \n%s" % line
                    for ign in ignore:
                        if ign in message:
                            print_message = None#'Debug Control'
                    if print_message is not None:
                        print(print_message)
            # INFO
            elif entry_type == 1:  # TRACE
                if message[:2] == "++":
                    trace_save.append((date_time_ob, message))
                elif message[:2] == "--":
                    trace_save.append((date_time_ob, message))
                elif message[:18] == 'DoFuncExecuteAsync':
                    trace_save.append((date_time_ob, message))
                elif message[0] == "f":  #finish tracing
                    if message[:4] == 'file':
                        trace_save.append((date_time_ob, message))
                        print("Trace")
                        #print(trace_save)
                        active_command.trace_command(trace_save)
                        trace_save = []
                    else:
                        #ignore formats
                        pass
                elif message.split(":")[0] == "open temp record":
                    if active_xml.temp_record_df is None:
                        active_xml.set_temp(date_time_ob, message, log_path)
                elif message[:17] == "-registerCallback":
                    trace_save.append((date_time_ob, message))
                else:
                    pass
            # TRACE
            elif entry_type == 2:  #this needs work
                print("Error, line %s"%i)
                try_split = message.split(":")
                if len(try_split) == 2:
                    active_xml.add_error(1, date_time_ob, try_split[1])
                else:
                    active_xml.add_error(1, date_time_ob, message)
            # ERROR
            elif entry_type == 3:
                print("Warning!!!!")
                active_xml.add_error(3, date_time_ob, message)
            # WARNING
            else:
                print("Wrong Entry Type? \n%s" % line)
                break
            # message_chars.append(message[0])  # first character of message, space is trimmed in simple_parse
        i += 1
    return xml_runs

def simple_parse_line(line):
    line = line.replace('"', '')
    line_arr = line.split("]")
    if len(line_arr) != 4:  #for log starters, sometimes "\n" or "reopen log file"
        return 1, None, None, None, None
    date_time = line_arr[0][1:] #trim bracket
    log_level = line_arr[1][1:]#trim bracket
    process = line_arr[2][1:]#trim bracket
    message = line_arr[3][1:-1]#trim space, newline
    return 0, date_time, log_level, process, message

def classify_loglevel(log_level):
    #only 2 log_levels currently.
    if log_level == 'info':
        return 0
    elif log_level == 'trace':
        return 1
    elif log_level == 'error':
        return 2
    elif log_level == 'warning':
        return 3
    else:
        return None

if __name__ == "__main__":
    logs = all_files(".log")
    print(logs)
    log = logs[0]
    temps = all_files(".csv")
    print(temps)
    t0 = time.perf_counter()
    xml_runs = analyze_logfile(log, None)
    t1 = time.perf_counter()
    print("Output:  \n")
    for xml_num, xmlr in zip(range(len(xml_runs)), xml_runs):
        print("XML Num: %s"%xml_num)
        script_res = {}
        #print(xmlr.script_dict)
        for scriptnum, script_tup in xmlr.script_dict.items():
            print("ScriptNum: %s"%scriptnum, script_tup[0])
            print(script_tup[1].execution_time)
            script_res[scriptnum] = [script_tup[0], script_tup[1].execution_time]
        script_rez = pd.DataFrame.from_dict(script_res, orient='index', columns=["Script Name", "Exec Time"]).to_html("xmlrun_%s.html"%xml_num)