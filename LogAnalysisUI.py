import kivy.resources
from kivy.app import App
from kivy.uix.floatlayout import FloatLayout
from kivy.factory import Factory
from kivy.uix.popup import Popup
from kivy.uix.label import Label
from kivy.properties import ListProperty, ObjectProperty, BooleanProperty
from kivy.uix.recycleview import RecycleView
from kivy.uix.recycleview.views import RecycleDataViewBehavior
from kivy.uix.recycleboxlayout import RecycleBoxLayout
from kivy.uix.behaviors import FocusBehavior
from kivy.uix.recycleview.layout import LayoutSelectionBehavior
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.gridlayout import GridLayout
from kivy.uix.button import Button
from kivy.uix.popup import Popup
from kivy.uix.screenmanager import Screen, ScreenManager
import threading
import queue
from LogFileParse import *
from ReadTools import *
import os
import sys


def resourcePath():
    '''Returns path containing content - either locally or in pyinstaller tmp file'''
    if hasattr(sys, '_MEIPASS'):
        return os.path.join(sys._MEIPASS)

    return os.path.join(os.path.abspath("."))


kivy.resources.resource_add_path(resourcePath()) # add this line

class LogSelectDialog(FloatLayout):
    selectfile = ObjectProperty(None)
    cancel = ObjectProperty(None)
    location = ObjectProperty(None)


class TempSelectDialog(FloatLayout):
    selectfile = ObjectProperty(None)
    cancel = ObjectProperty(None)
    location = ObjectProperty(None)


class LogChooser(Screen):
    logfile_path = ObjectProperty(None)
    log_check = None
    temp_paths = ObjectProperty(None)
    temp_steps = ObjectProperty()
    auto_temp = True

    def __init__(self, **kwargs):
        super(LogChooser, self).__init__(**kwargs)

    def dismiss_popup(self):
        self._popup.dismiss()

    def show_select(self):
        loc = cwd
        content = LogSelectDialog(selectfile=self.select, cancel=self.dismiss_popup, location =loc)
        self._popup = Popup(title="Choose Log to Read:", content=content,
                            size_hint=(0.9, 0.9))
        self._popup.open()

    def show_temp(self):
        loc = cwd
        content = TempSelectDialog(selectfile = self.temp, cancel=self.dismiss_popup, location = loc)
        self._popup = Popup(title="Choose Temp Data File(s):", content=content,
                            size_hint=(0.9, 0.9))
        self._popup.open()

    def select(self, path):
        print(path)
        self.logfile_path = path[0]
        file = self.logfile_path.split("\\")[-1]
        self.ids.selectedlog.text = str(file)
        self.log_check = True
        self.dismiss_popup()

    def temp(self, path):
        self.auto_temp = False
        print(path)
        self.temp_paths = path
        disp_str = self.make_temp_str()
        self.ids.tempstr.text = disp_str
        self.dismiss_popup()

    def manual_temp(self):
        self.ids.manbutton.disabled = False

    def make_temp_str(self):
        files = []
        for temp in self.temp_paths:
            file = temp.split("\\")[-1]
            files.append(file)
        return "\n".join(files)

    def get_tempsteps(self):
        input = App.get_running_app().root.ids.chooser.ids.tempstp.text
        try:
            step = int(input)
        except ValueError:
            step = 10
        self.temp_steps = step

    def pass_settings(self):
        self.get_tempsteps()
        print(self.logfile_path)
        if self.log_check == None:
            pass
        else:
            App.get_running_app().root.transition.direction = "left"
            App.get_running_app().root.duration = 0.1
            App.get_running_app().root.current = 'logprocess'
            App.get_running_app().root.ids.process.log = self.logfile_path
            App.get_running_app().root.ids.process.temps = self.temp_paths
            App.get_running_app().root.ids.process.auto_temp = self.auto_temp
            App.get_running_app().root.ids.process.tempsave_step = self.temp_steps
            App.get_running_app().root.ids.process.startread()
            App.get_running_app().root.ids.process.read_que_thread()


class ProcessOutput(Screen):
    cycle_type_df = ObjectProperty()
    cycletype_data = {}
    xml_error_df = ObjectProperty()
    select_seg = ObjectProperty()
    log = ObjectProperty()
    temps = ObjectProperty()
    tempsave_step = ObjectProperty()
    xmlruns = ObjectProperty()
    auto_temp = True

    def __init__(self, **kwargs):
        super(ProcessOutput, self).__init__(**kwargs)
        self.cycle_type = [{'segment':{'text': 'Log Script Segments'}}]

    def updatestatus(self, value, statustext):
        self.ids.statustext.text = str(statustext)
        self.ids.status.value = value

    def populate(self):
        valuelist = self.cycle_type
        segments = list(self.cycletype_data.keys())
        if len(segments) >0:
            selected_segment = segments[self.select_seg]
            print(selected_segment)
        #self.ids.rv.data = sorted(valuelist, key = lambda k: k['segment']['text'])

    def logfileparse_run(self):
        self.cycle_type_df, self.cycletype_data, self.xml_error_df, self.xmlruns = main_run(self.log, self.temps, self.auto_temp, App.get_running_app().root.out_queue, self.tempsave_step)
        App.get_running_app().root.out_queue.put(['Compile Complete Displaying Runs', 100], block=False)
    def readthread(self):
        t = threading.Thread(target=self.logfileparse_run)
        t.daemon = True
        t.start()

    def startread(self):
        self.readthread()

    def read_queue(self):
        queue = App.get_running_app().root.out_queue
        while True:
            try:
                packet = queue.get(block=False)
                print(packet)
                #parse packet
                stat_str = packet[0]
                compl_val = packet[1]
            except Exception as e:
                print("Que Exception %s"%e)
                time.sleep(0.5)
                continue
            if type(compl_val) is tuple:
                x = compl_val[0]
                y = compl_val[1]
                base = 20
                ratio = x/y
                add = ratio*70
                ret_val = int(base + add)
                print(x, y, ret_val)
                self.updatestatus(ret_val, stat_str)
            elif compl_val in [10, 20, 95, 100]:
                self.updatestatus(compl_val, stat_str)
                if compl_val == 100:
                    self.update_listview()
                    return 0
            else:
                print("Unknown item in queue, Should not get here")
    def read_que_thread(self):
        rqt = threading.Thread(target=self.read_queue)
        rqt.daemon = True
        rqt.start()

    def update_listview(self):
        print(self.cycle_type_df, self.cycletype_data, self.xml_error_df)
        # scrolling left
        # pull arrays from df
        xmlrunnums = self.cycle_type_df['XML Runs'].values
        runcyclenums = self.cycle_type_df['Run Cycle'].values
        cycletypestrs = self.cycle_type_df['Cycle Type'].values
        # restructure to reflect label layout
        rvdata = []
        for xml, cycle, type in zip(xmlrunnums, runcyclenums, cycletypestrs):
            d = {'xml': {'text': str(xml)}, 'cycle': {'text': str(cycle)}, 'type': {'text': str(type)}}
            rvdata.append(d)
        # set to scrolling left rv data
        App.get_running_app().root.ids.process.ids.rv.data = rvdata
        # Selectable Right
        segments = list(self.cycletype_data.keys())
        # make relev xml list by segment elem
        relev_xmls = []
        for seg in segments:
            filter_seg = self.cycle_type_df.loc[self.cycle_type_df['Cycle Type'] == seg, :]
            nums = np.unique(filter_seg['XML Runs'].values)
            relev_xmls.append(str(nums))
        # restucture
        print(segments, relev_xmls)
        selrvdata = []
        for seg, nums in zip(segments, relev_xmls):
            d = {'segment': {'text': seg}, 'xmls': {'text': nums}}
            selrvdata.append(d)
        # set to selectable right
        App.get_running_app().root.ids.process.ids.segrv.data = selrvdata
        App.get_running_app().root.ids.process.ids.runreport.disabled = False
        App.get_running_app().root.ids.process.ids.cyclesheets.disabled = False

    def output_runreport(self):
        print(self.select_seg)
        if self.select_seg == None:
            print("select log segment")
            App.get_running_app().root.ids.process.ids.statustext.text = 'Select Log Segment'
            return
        segments = list(self.cycletype_data.keys())
        value = segments[self.select_seg]
        relev_xml = self.cycle_type_df.loc[self.cycle_type_df['Cycle Type'] == value, :]
        uniqe_xml_num = np.unique(relev_xml['XML Runs'].values)
        relev_xml_errors = pd.DataFrame()
        for val in uniqe_xml_num:
            relev_xml_errors = pd.concat([relev_xml_errors, self.xml_error_df[val]], axis=0)
        relev_xml_errors.sort_values(by=['DateTime'], inplace=True, ascending=True)
        overview_df, mod_cmd_sum_df, module_sum_df, procedure_cmd_dict, cmd_temp_stats_df, num_cmds, proc_name_lst, types_cycle, temp_save_df, proc_temp_dfs, proc_error_df = \
        self.cycletype_data[value]
        # check nan time:  overview_df
        # reset index on proc_error_df, relev_xml_errors
        overview_df, proc_error_df, relev_xml_errors = close_data_forOutput(overview_df, proc_error_df,
                                                                            relev_xml_errors)
        output_RunResults_excel(value, overview_df, procedure_cmd_dict,
                                cmd_temp_stats_df, mod_cmd_sum_df, module_sum_df, proc_name_lst, temp_save_df,
                                proc_temp_dfs, relev_xml_errors, proc_error_df)

    def output_cyclesheets(self):
        if self.select_seg == None:
            print("select log segment")
            App.get_running_app().root.ids.process.ids.statustext.text = 'Select Log Segment'
            return
        self.thread_cyclesheets()
        self.read_que_thread()


    def run_cyclesheets(self):
        print(self.select_seg)
        if self.select_seg == None:
            print("select log segment")
            App.get_running_app().root.out_queue.put(['Select Log Segment', 100], block=False)
            return
        App.get_running_app().root.ids.process.ids.runreport.disabled = True
        App.get_running_app().root.ids.process.ids.cyclesheets.disabled = True
        segments = list(self.cycletype_data.keys())
        value = segments[self.select_seg]
        output_by_cycle_que(self.xmlruns, value, self.log, App.get_running_app().root.out_queue)
        App.get_running_app().root.out_queue.put(['CycleSheet Output Complete', 100], block=False)
        App.get_running_app().root.ids.process.ids.runreport.disabled = False
        App.get_running_app().root.ids.process.ids.cyclesheets.disabled = False

    def thread_cyclesheets(self):
        tcss = threading.Thread(target=self.run_cyclesheets)
        tcss.daemon = True
        tcss.start()

    def reset_app_newlog(self):
        proc_scrn = App.get_running_app().root.ids.process
        choose_scrn = App.get_running_app().root.ids.chooser
        #reset proc_scrn
        proc_scrn.cycle_type_df = ObjectProperty()
        proc_scrn.cycletype_data = {}
        proc_scrn.xml_error_df = ObjectProperty()
        proc_scrn.xmlruns = ObjectProperty()
        proc_scrn.select_seg = ObjectProperty(None)
        proc_scrn.log = ObjectProperty()
        proc_scrn.temps = ObjectProperty()
        proc_scrn.tempsave_step = ObjectProperty()
        proc_scrn.cycle_type = [{'segment':{'text': 'Log Script Segments'}}]
        proc_scrn.auto_temp = True
        App.get_running_app().root.out_queue.put(['Status Text', 10], block = False)
        proc_scrn.ids.rv.data = [{'xml': {'text': ''}, 'cycle':{'text': ''}, 'type': {'text': ''}}]
        proc_scrn.ids.segrv.data = [{'segment':{'text': ''}, 'xmls': {'text': ''}}]
        proc_scrn.ids.runreport.disabled = True
        proc_scrn.ids.cyclesheets.disabled = True
        #choose scrn reset
        #print(proc_scrn.ids)
        choose_scrn.log_check = None
        choose_scrn.ids.selectedlog.text = ''
        choose_scrn.ids.autotemp.active = True
        choose_scrn.ids.manbutton.disabled = True
        choose_scrn.ids.tempstr.text = 'Auto\nSame Directory as Log'
        choose_scrn.ids.tempstp.text = '20'
        choose_scrn.logfile_path = ObjectProperty(None)
        choose_scrn.temp_paths = ObjectProperty(None)
        choose_scrn.temp_steps = ObjectProperty()
        choose_scrn.auto_temp = True


class RV(RecycleView):
    def __init__(self, **kwargs):
        super(RV, self).__init__(**kwargs)
        self.data = [{'xml': {'text': ''}, 'cycle':{'text': ''}, 'type': {'text': ''}}]

class SegRV(RecycleView):
    def __init__(self, **kwargs):
        super(SegRV, self).__init__(**kwargs)
        self.data = [{'segment':{'text': ''}, 'xmls': {'text': ''}}]


class SelectableRecycleBoxLayout(FocusBehavior, LayoutSelectionBehavior, RecycleBoxLayout):
    ''' Adds selection and focus behaviour to the view. '''

class SelectableLabel(RecycleDataViewBehavior, GridLayout):
    ''' Add selection support to the Label '''
    index = None
    selected = BooleanProperty(False)
    selectable = BooleanProperty(True)
    cols = 2

    def refresh_view_attrs(self, segrv, index, data):
        ''' Catch and handle the view changes '''
        self.index = index
        self.ids.seg_label.text = data['segment']['text']
        self.ids.relevxml_label.text = data['xmls']['text']
        return super(SelectableLabel, self).refresh_view_attrs(segrv, index, data)

    def on_touch_down(self, touch):
        ''' Add selection on touch down '''
        if super(SelectableLabel, self).on_touch_down(touch):
            return True
        if self.collide_point(*touch.pos) and self.selectable:
            return self.parent.select_with_touch(self.index, touch)

    def apply_selection(self, approot, index, is_selected):
        ''' Respond to the selection of items in the view. '''
        self.selected = is_selected
        if is_selected:
            App.get_running_app().root.ids.process.select_seg = index
            App.get_running_app().root.ids.process.populate()

class DoubleLabel(RecycleDataViewBehavior, GridLayout):
    index = None
    selected = BooleanProperty(False)
    selectable = BooleanProperty(False)
    cols = 3

    def refresh_view_attrs(self, rv, index, data):
        ''' Catch and handle the view changes '''
        self.index = index
        self.ids.xml_label.text = data['xml']['text']
        self.ids.cyc_label.text = data['cycle']['text']
        self.ids.typ_label.text = data['type']['text']
        return super(DoubleLabel, self).refresh_view_attrs(rv, index, data)

class UIScreenManager(ScreenManager):
    in_queue = queue.Queue()
    out_queue = queue.Queue()

class LogAnalysisUI(App):


    def build(self):
        self.title = "Ecoli Log Analysis App"
        root_sm = UIScreenManager()
        return root_sm

Factory.register('UIScreenManager', cls=UIScreenManager)
Factory.register('LogChooser', cls=LogChooser)
Factory.register('ProcessOutput', cls=ProcessOutput)
Factory.register('LogSelectDialog', cls=LogSelectDialog)

cwd = os.getcwd()

if __name__=="__main__":
    print("Starting")
    BasicUI = LogAnalysisUI()
    BasicUI.run()

    try:
        sys.stdout.close()
    except:
        pass
    try:
        sys.stderr.close()
    except:
        pass

