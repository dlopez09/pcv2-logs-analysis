# -*- mode: python -*-

import sys
sys.setrecursionlimit(5000)


from kivy_deps import sdl2, glew

block_cipher = None


a = Analysis(['LogAnalysisUI.py'],
             pathex=['C:\\Users\\dlopez\\Desktop\\Work\\IPV\\NewCodeRepo\\LogAnalysisToolUI_git\\pcv2-logs-analysis\\'],
             binaries=[],
             datas=[],
             hiddenimports=['win32file', 'win32timezone'],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
a.datas += [('LogAnalysisUI.kv', 'C:\\Users\\dlopez\\Desktop\\Work\\IPV\\NewCodeRepo\\LogAnalysisToolUI_git\\pcv2-logs-analysis\\LogAnalysisUI.kv', 'DATA')]
a.datas += [('LogFileParse.py', 'C:\\Users\\dlopez\\Desktop\\Work\\IPV\\NewCodeRepo\\LogAnalysisToolUI_git\\pcv2-logs-analysis\\LogFileParse.py', 'DATA')]
a.datas += [('ReadTools.py', 'C:\\Users\\dlopez\\Desktop\\Work\\IPV\\NewCodeRepo\\LogAnalysisToolUI_git\\pcv2-logs-analysis\\ReadTools.py', 'DATA')]
exe = EXE(pyz, Tree('C:\\Users\\dlopez\\Desktop\\Work\\IPV\\NewCodeRepo\\LogAnalysisToolUI_git\\pcv2-logs-analysis\\'),
          a.scripts,
          a.binaries,
          a.zipfiles,
          a.datas,
          *[Tree(p) for p in (sdl2.dep_bins + glew.dep_bins)],
          name='Ecoli_LogAnalysisTool_1.1',
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          runtime_tmpdir=None,
          console=False )
