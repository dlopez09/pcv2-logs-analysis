import os
from shutil import move
import pandas as pd
import numpy as np
import xlrd
import time
from math import isnan
import natsort
from ReadTools import *
from xlsxwriter.utility import xl_rowcol_to_cell, xl_cell_to_rowcol, xl_col_to_name
from datetime import datetime, timedelta
import queue

version = '0.5.1'

mark = ["o", "^", "2", "s", "p" , "P", "*", "X", "D"]

colors = ['black', 'blue', 'brown', 'cyan',
          'gray', 'green', 'lime', 'magenta',
          'navy', 'orange', 'pink', 'purple', 'red', 'silver', 'yellow']

def enum_logs():
    # Choose Log
    logs = all_files(".log")
    print("Enter #", "\t", "Log")
    for count, ele in list(enumerate(logs)):
        print(count, "\t", ele)
    return logs

def output_cleanup(fname_lst, log):
    rm_suffix = log[:-4]
    #print("Folder Name: %s"%rm_suffix)
    try:
        os.mkdir(rm_suffix)
    except FileExistsError:
        pass
    for file in fname_lst:
        # print("Moving %s to %s"%(file, rm_suffix+'/'+file))
        move(file, rm_suffix + '/' + file)

def compile_from_closedscripts(xml_runs, step):
    #compile dfs/dicts
    cycle_type_data_dict = {}
    cycle_type_dict = {}
    if type(step) is not int:
        try:
            step = int(step)
        except TypeError:
            step = 20
    total_scripts = 0
    xml_errors = []
    for xml_num, xmlr in zip(range(len(xml_runs)), xml_runs):
        print("XML Num: %s" % xml_num)
        # print(xmlr.script_dict)
        xml_error_df = xmlr.check_errors()
        for scriptnum, script_tup in xmlr.script_dict.items():
            print("ScriptNum: %s" % scriptnum, script_tup[0])
            print(script_tup)
        #print(first_cycle, last_cycle, num_cycles, step)
            print("Compiling Output Closed XML %s, ScriptCycle %s" %(xml_num,scriptnum))
            print(script_tup[1].cycle)
            print([proc.name for proc in script_tup[1].procedures])
            cycle_type, overview_proc_df = script_tup[1].script_return_df(scriptnum)  #looks clear of glaring errors
            if cycle_type == 'Manual_Default_Script':
                continue
            if cycle_type in cycle_type_data_dict:
                overview_df = cycle_type_data_dict[cycle_type][0]
                mod_cmd_sum_df = cycle_type_data_dict[cycle_type][1]
                module_sum_df = cycle_type_data_dict[cycle_type][2]
                procedure_cmd_dict = cycle_type_data_dict[cycle_type][3]
                command_temp_stats_dict = cycle_type_data_dict[cycle_type][4]
                c = cycle_type_data_dict[cycle_type][5]
                types_cycle = cycle_type_data_dict[cycle_type][7]
                types_cycle += 1
                temp_save_df = cycle_type_data_dict[cycle_type][8]
                proc_temp_dfs = cycle_type_data_dict[cycle_type][9]
                procedure_error_df = cycle_type_data_dict[cycle_type][10]
            else:
                # [overviewdf, mod_cmd_sum_df, module_sum_df, procedure_cmd_dict, command_temp_stats_dict]
                overview_df = pd.DataFrame()
                mod_cmd_sum_df = pd.DataFrame()
                module_sum_df = pd.DataFrame()
                procedure_cmd_dict = {}
                command_temp_stats_dict = {}
                c = 0
                types_cycle = 0
                temp_save_df = pd.DataFrame()
                proc_temp_dfs = {}
                procedure_error_df = pd.DataFrame(columns=['Error Type', 'DateTime', 'Message/Returns', 'Cycle'])
            overview_df = pd.concat([overview_df, overview_proc_df], ignore_index=True)
            names = []
            rep = 1
            cycle = script_tup[1].cycle
            cycle_type_dict[total_scripts] = [xml_num,cycle, cycle_type]
            total_scripts += 1
            temp_df = script_tup[1].temp_record_df
            grp_cmd_df = pd.DataFrame()
            if types_cycle%step == 0:
                temp = temp_df.dropna()
                temp.columns = [str(col)+'_'+str(scriptnum) for col in temp.columns]
                temp_save_df = pd.concat([temp_save_df, temp.reset_index(drop=True)], axis=1)
                print("Cycle temp saved as: %s "%scriptnum)

            for proc in script_tup[1].procedures:
                if proc.name is None:
                    print("Procedure Name is None.  This should not happen.")
                # check repeat proc names!
                if proc.name in names:
                    if len(proc.name) >= 31:
                        proc.name = proc.name[:28] + "_%s" % rep
                        rep += 1
                    else:
                        proc.name = proc.name + "_%s" % rep
                        rep += 1
                    names.append(proc.name)
                else:
                    names.append(proc.name)
                proc_df, proc_grp_cmdmod_df, trimmed_temp_df, error_df = proc.proc_return_df(temp_df)  #looks clear of glaring errors
                if not error_df.empty:
                    error_df.loc[:, 'Cycle'] = cycle
                    error_df.loc[:, 'Procedure'] = proc.name
                    procedure_error_df = pd.concat([procedure_error_df, error_df], axis=0)
                    xml_error_df = pd.concat([xml_error_df, error_df], axis=0)
                grp_cmd_df = pd.concat([grp_cmd_df, proc_grp_cmdmod_df], axis=0)
                command_info = proc_df.iloc[:, :3]
                if proc.name not in procedure_cmd_dict:
                    command_info.loc[:, 'Command_in_Cycle'] = 1
                    procedure_cmd_dict[proc.name] = command_info
                    temp_data_df = trimmed_temp_df.loc[trimmed_temp_df['Sensor Temp'] != 0, :]
                    temp_data_df = temp_data_df.dropna()
                else:
                    proc_cmd_dictdf = procedure_cmd_dict[proc.name]
                    proc_cmd_df = proc_cmd_dictdf.loc[:, proc_cmd_dictdf.columns != 'Command_in_Cycle']
                    if proc_cmd_df.equals(command_info):
                        procedure_cmd_dict[proc.name]['Command_in_Cycle'] +=1
                        temp_data_df = trimmed_temp_df.loc[trimmed_temp_df['Sensor Temp'] != 0, :]
                        temp_data_df = temp_data_df.dropna()
                    else:
                        cmd_info_idx = len(command_info.index.values)
                        proc_cmd_df_idx = len(proc_cmd_df.index.values)
                        cic = procedure_cmd_dict[proc.name]['Command_in_Cycle'].values
                        #print(cic)
                        if cmd_info_idx > proc_cmd_df_idx:
                            compare_result = command_info.eq(proc_cmd_df)
                            false = compare_result.loc[compare_result['Module'] == False, :].index
                            tru = compare_result.loc[compare_result['Module'] == True, :].index
                            #print(false)
                            diff_row = command_info.loc[command_info.index.isin(false),:]
                            print(diff_row)
                            diff_row  = diff_row.assign(Command_in_Cycle = 1)
                            proc_cmd_dictdf = pd.concat([proc_cmd_dictdf, diff_row])
                        else:
                            compare_result = proc_cmd_df.eq(command_info)
                            #false = compare_result.loc[compare_result['Module'] == False, :].index
                            tru = compare_result.loc[compare_result['Module'] == True, :].index
                        proc_cmd_dictdf.loc[proc_cmd_dictdf.index.isin(tru), 'Command_in_Cycle'] += 1
                        procedure_cmd_dict[proc.name] = proc_cmd_dictdf
                        temp_data_df = trimmed_temp_df.loc[trimmed_temp_df['Sensor Temp'] != 0, :]
                        temp_data_df = temp_data_df.dropna()
                if types_cycle%step == 0:
                    temp = temp_data_df.dropna()  #start save proc temp data
                    temp.columns = [str(col) + '_' + str(scriptnum) for col in temp.columns]   #append cycle number to columns
                    try:
                        prev_df = proc_temp_dfs[proc.name]  #try extract previous saved proc df
                    except KeyError:
                        prev_df = pd.DataFrame()   #none exists, make one
                    temp = pd.concat([prev_df, temp.reset_index(drop=True)], axis =1)  #concat new data
                    proc_temp_dfs[proc.name] = temp
                for cmd_idx, start, end in zip(command_info.index.values, proc_df['Start Time'].values, proc_df['End Time'].values):
                    if len(temp_data_df.index) == 0:
                        command_temp_stats_dict[c] = [scriptnum, proc.name, cmd_idx, end - start,
                                                      None, None, None, None,
                                                      None, None, None, None]
                    else:
                        temp_segment = temp_data_df.loc[temp_data_df['DateTime'].values >= start]
                        temp_segment = temp_segment.loc[temp_segment['DateTime'].values <= end]
                        if len(temp_segment.index) > 0:
                            tec = temp_segment['TEC Temp'].values
                            sensor = temp_segment['Sensor Temp'].values
                            tec_mean = np.mean(tec)
                            tec_std = np.std(tec)
                            tec_max = np.amax(tec)
                            tec_min = np.amin(tec)
                            sens_mean = np.mean(sensor)
                            sens_std = np.std(sensor)
                            sens_max = np.amax(sensor)
                            sens_min = np.amin(sensor)
                            command_temp_stats_dict[c] = [scriptnum, proc.name, cmd_idx, end - start,
                                                          tec_mean, tec_std, tec_max, tec_min,
                                                          sens_mean, sens_std, sens_max, sens_min]
                        else:
                            command_temp_stats_dict[c] = [scriptnum, proc.name, cmd_idx, end - start,
                                                          None, None, None, None,
                                                          None, None, None, None]
                    c += 1
            #print(grp_cmd_df, i)
            modcmd_sum_byproc_df = make_modcmd_sum_by_proc(grp_cmd_df)#  summarized module_cmd type(cmos_image) by procedure
            cyc_modcmd_overview_df = make_cycle_modcmd_overview(modcmd_sum_byproc_df) #summarized module_cmd type(cmos_image) by cycle
            modcmd_sum_byproc_df['Elapsed Time'] = [timedelta(microseconds= 1000) if x is None else timedelta(microseconds= x / 1000) for x in modcmd_sum_byproc_df['Elapsed Time'].values]  #script summarize elapsed time total
            modcmd_sum_byproc_df['Cycle'] = scriptnum
            mod_cmd_sum_df = pd.concat([mod_cmd_sum_df, modcmd_sum_byproc_df])
            cyc_modcmd_overview_df['Total Time'] = [timedelta(microseconds= 1000) if x is None else timedelta(microseconds= x / 1000) for x in cyc_modcmd_overview_df['Total Time'].values]
            cyc_modcmd_overview_df['Cycle'] = scriptnum
            module_sum_df = pd.concat([module_sum_df, cyc_modcmd_overview_df])
            #print(grp_cmd_df, i, modcmd_sum_byproc_df)
            #print(i,cyc_modcmd_overview_df, modcmd_sum_byproc_df)
            cycle_type_data_dict[cycle_type] = [overview_df, mod_cmd_sum_df, module_sum_df, procedure_cmd_dict, command_temp_stats_dict, c, names, types_cycle, temp_save_df, proc_temp_dfs, procedure_error_df]
        xml_errors.append(xml_error_df)
    for cycle_type in cycle_type_data_dict:
        cycle_type_data_dict[cycle_type][4] = pd.DataFrame.from_dict(cycle_type_data_dict[cycle_type][4], orient='index',columns=['Cycle', 'Procedure', 'Command', 'Exec. Time',
                                                        'TEC_Mean', 'TEC_Std', 'TEC_Max', 'TEC_Min',
                                                        'Sensor_Mean', 'Sensor_Std', 'Sensor_Max', 'Sensor_Min'])
    cycle_type_df = pd.DataFrame.from_dict(cycle_type_dict, orient='index', columns=['XML Runs','Run Cycle', 'Cycle Type'])
    cycle_type_df.index.name = 'Cycle Order'
    return cycle_type_df, cycle_type_data_dict, xml_errors

def output_all_closed(closed_scripts_lst, log):
    output_files = []
    for i in range(len(closed_scripts_lst)):
        print("Output Closed ScriptCycle %s"%i)
        writer, workbook, fname = closed_scripts_lst[i].script_output_df(i)
        output_files.append(fname)
        names= []
        rep = 1
        temp_df = closed_scripts_lst[i].temp_record_df
        grp_cmd_df = pd.DataFrame()
        for proc in closed_scripts_lst[i].procedures:
            #check repeat proc names!
            if proc.name in names:
                if len(proc.name)>=31:
                    proc.name = proc.name[:28]+"_%s"%rep
                    rep+=1
                else:
                    proc.name = proc.name+"_%s"%rep
                    rep+=1
                names.append(proc.name)
            else:
                names.append(proc.name)
            writer, workbook, proc_grp_cmdmod_df = proc.proc_output_df(writer, workbook, temp_df)
            grp_cmd_df = pd.concat([grp_cmd_df, proc_grp_cmdmod_df], axis=0)
        writer, workbook = output_grp_modcmd_sheet(grp_cmd_df, writer, workbook)
        writer.save()
        output_cleanup(output_files, log)
        output_files = []

def output_by_cycle(xml_runs, segment, log):

    output_files = []
    for xml_num, xmlr in zip(range(len(xml_runs)), xml_runs):
        print("XML Num: %s" % xml_num)
        for scriptnum, script_tup in xmlr.script_dict.items():
            print("ScriptNum: %s, Segment: %s" % (scriptnum, script_tup[0]))
            rm_suff = script_tup[0][:-4]
            seg_compare = "_".join(script_tup[0].split(" "))
            if segment in seg_compare:
                writer, workbook, fname = script_tup[1].script_output_df(scriptnum, xml_num)
                names = []
                rep = 1
                temp_df = script_tup[1].temp_record_df
                grp_cmd_df = pd.DataFrame()
                for proc in script_tup[1].procedures:
                    # check repeat proc names!
                    if proc.name in names:
                        if len(proc.name) >= 31:
                            proc.name = proc.name[:28] + "_%s" % rep
                            rep += 1
                        else:
                            proc.name = proc.name + "_%s" % rep
                            rep += 1
                        names.append(proc.name)
                    else:
                        names.append(proc.name)
                    writer, workbook, proc_grp_cmdmod_df = proc.proc_output_df(writer, workbook, temp_df)
                    grp_cmd_df = pd.concat([grp_cmd_df, proc_grp_cmdmod_df], axis=0)
                writer, workbook = output_grp_modcmd_sheet(grp_cmd_df, writer, workbook)
                writer.save()
                output_files.append(fname)
    output_cleanup(output_files, log)

def make_result_categories(logs, log_num):
    home  = os.getcwd()
    log_output_dir = home+"/"+logs[log_num][:-4]
    os.chdir(log_output_dir)
    excel_files = all_files(".xlsx")
    groups = {}
    i = 0
    for file in excel_files:
        segments = file.split("_")
        cycle = segments[2]
        iso_seg = "_".join(segments[3:-1]+ [segments[-1][:-5]])
        groups[i] = [cycle, iso_seg, file]
        i+=1
    groups_df = pd.DataFrame.from_dict(groups, orient = 'index', columns=['Cycle', 'Segment', 'File'])
    values, counts = np.unique(groups_df['Segment'].values, return_counts = True)
    for count, ele in list(enumerate(values)):
        print(count, "\t", ele)
    os.chdir(home)
    return values, groups_df

def compile_proc_cmds(cycles, logs, log_num):
    """Compiles results from RunScriptANalysis Files (by cycle)
    Input:  Cycles: list[] of cycle .xlsx RunScriptAnalysis files
            Logs:  list[] of log files present in folder
            log_num:  index of selected log, compiles results of selected log
    Output:  procedure_cmd_dict: Dictionary{} of all commands in cycle, key: Procedure name, value: command_df includes [Module, Module CMD Type, CMD Params]
            cmd_temp_stats_df:  df of trimmed temp data for each command, includes procedure and cycle data
            overview_df:  Time details for the run, procedure level
            """
    home = os.getcwd()
    log_output_dir = home + "/" + logs[log_num][:-4]
    os.chdir(log_output_dir)
    overview_df = pd.DataFrame()
    mod_cmd_sum_df = pd.DataFrame()
    module_sum_df = pd.DataFrame()
    i=0
    procedure_cmd_dict = {}
    command_temp_stats_dict = {}
    c=0
    for imp in cycles:
        #print(imp)
        xls = xlrd.open_workbook(imp, on_demand = True)
        sheet_names = xls.sheet_names()
        cycle = int(imp.split("_")[2])
        for sheet in sheet_names:
            #print(sheet)
            if "Overview" in sheet:
                info_df = pd.read_excel(imp, sheet_name=sheet, index_col=[0], usecols=[0,1])
                numproc = int(info_df.values[-1])
                overview_proc_df = pd.read_excel(imp, sheet_name=sheet, index_col=[0], usecols=range(4, (5+numproc)), nrows = 5 ).T
                overview_proc_df['Execution Time\n(HH:MM:SS:sss)'] = overview_proc_df['End Time'] - overview_proc_df['Start Time']
                overview_proc_df = overview_proc_df.drop(['Start Time', 'End Time'], axis = 1)
                overview_proc_df['Procedure'] = overview_proc_df.index
                overview_proc_df = overview_proc_df.reset_index(drop = True)
                overview_proc_df['Cycle\n(Number)'] = cycle
                overview_df = pd.concat([overview_df, overview_proc_df], ignore_index = True)
            elif "Temp Data" in sheet:
                pass
            elif "Module Command Summary" in sheet:
                #pass#  TO BE USED TO COMPILE MODULE COMMAND SUMMARIES FOR STAT ANALYSIS DOWNSTREAM
                mod_cmd_summ = pd.read_excel(imp, sheet_name=sheet, index_col=[0], usecols=range(0, 5))
                mod_cmd_summ['Cycle'] = cycle
                mod_cmd_sum_df = pd.concat([mod_cmd_sum_df,mod_cmd_summ])
                mod_sum = pd.read_excel(imp, sheet_name=sheet, index_col=None, usecols = range(6,9))
                mod_sum = mod_sum.dropna()
                mod_sum['Cycle'] = cycle
                module_sum_df = pd.concat([module_sum_df, mod_sum])
            else:
                proc_name = sheet
                #basic_info = pd.read_excel(imp, sheet_name=sheet, index_col=[0], usecols=[0,1], nrows= 9)
                cmd_res = pd.read_excel(imp, sheet_name=sheet, index_col=[0], usecols=range(3,11))
                cmd_res['Execution Time'] = cmd_res['End Time'] - cmd_res['Start Time']
                cmd_res = cmd_res.dropna()
                command_info = cmd_res.iloc[:,:3].reset_index(drop=True)
                #command_info
                #cmd_res
                #Check if Command Info is the same for each procedure (all cycles), should always be true
                if i==0:
                    procedure_cmd_dict[proc_name] = command_info
                    temp_data_df = pd.read_excel(imp, sheet_name=sheet, indexcol = [0], usecols = range(0, 3), skiprows=range(0, 14))
                    temp_data_df = temp_data_df.loc[temp_data_df['Sensor Temp'] != 0, :]
                    temp_data_df = temp_data_df.dropna()
                    for cmd_idx, start, end in zip(command_info.index.values, cmd_res['Start Time'].values, cmd_res['End Time'].values):
                        #print(cmd_idx, start, end)
                        #print(cmd_idx, temp_data_df)
                        if len(temp_data_df.index) == 0:
                            command_temp_stats_dict[c] = [cycle, proc_name, cmd_idx, end - start,
                                                          None, None, None, None,
                                                          None, None, None, None]
                        else:
                            temp_segment = temp_data_df.loc[temp_data_df['DateTime'].values>=start]
                            temp_segment = temp_segment.loc[temp_segment['DateTime'].values<=end]
                            if len(temp_segment.index) > 0:
                                tec = temp_segment['TEC Temp'].values
                                sensor = temp_segment['Sensor Temp'].values
                                tec_mean = np.mean(tec)
                                tec_std = np.std(tec)
                                tec_max = np.amax(tec)
                                tec_min = np.amin(tec)
                                sens_mean = np.mean(sensor)
                                sens_std = np.std(sensor)
                                sens_max = np.amax(sensor)
                                sens_min = np.amin(sensor)
                                command_temp_stats_dict[c] = [cycle, proc_name, cmd_idx, end-start,
                                                              tec_mean, tec_std, tec_max, tec_min,
                                                              sens_mean, sens_std, sens_max, sens_min]
                            else:
                                command_temp_stats_dict[c] = [cycle, proc_name, cmd_idx, end-start,
                                                              None, None, None, None,
                                                              None, None, None, None]
                        c+=1
                else:
                    if procedure_cmd_dict[proc_name].equals(command_info):
                        temp_data_df = pd.read_excel(imp, sheet_name=sheet, indexcol = [0], usecols = range(0, 3), skiprows=range(0, 14))
                        temp_data_df = temp_data_df.loc[temp_data_df['Sensor Temp'] != 0, :]
                        temp_data_df = temp_data_df.dropna()
                        for cmd_idx, start, end in zip(command_info.index.values, cmd_res['Start Time'].values, cmd_res['End Time'].values):
                            if len(temp_data_df.index) == 0:
                                command_temp_stats_dict[c] = [cycle, proc_name, cmd_idx, end - start,
                                                              None, None, None, None,
                                                              None, None, None, None]
                            else:
                                temp_segment = temp_data_df.loc[temp_data_df['DateTime'].values >= start]
                                temp_segment = temp_segment.loc[temp_segment['DateTime'].values <= end]
                                if len(temp_segment.index) > 0:
                                    tec = temp_segment['TEC Temp'].values
                                    sensor = temp_segment['Sensor Temp'].values
                                    tec_mean = np.mean(tec)
                                    tec_std = np.std(tec)
                                    tec_max = np.amax(tec)
                                    tec_min = np.amin(tec)
                                    sens_mean = np.mean(sensor)
                                    sens_std = np.std(sensor)
                                    sens_max = np.amax(sensor)
                                    sens_min = np.amin(sensor)
                                    command_temp_stats_dict[c] = [cycle, proc_name, cmd_idx, end - start,
                                                                  tec_mean, tec_std, tec_max, tec_min,
                                                                  sens_mean, sens_std, sens_max, sens_min]
                                else:
                                    command_temp_stats_dict[c] = [cycle, proc_name, cmd_idx, end - start,
                                                                  None, None, None, None,
                                                                  None, None, None, None]
                            c+=1
                    else:
                        print(1)
                        #print(procedure_cmd_dict[proc_name], command_info)
        i+=1
    #overview_df
    #procedure_cmd_dict['Antibody Incorporation Preparation'] = procedure_cmd_dict.pop('Antibody Incorporation Prepara')
    cmd_temp_stats_df = pd.DataFrame.from_dict(command_temp_stats_dict, orient='index', columns=['Cycle', 'Procedure', 'Command', 'Exec. Time',
                                                                                                 'TEC_Mean', 'TEC_Std', 'TEC_Max', 'TEC_Min',
                                                                                                 'Sensor_Mean', 'Sensor_Std', 'Sensor_Max', 'Sensor_Min'])
    os.chdir(home)
    overview_df = overview_df.drop(['Execution Time'], axis=1)
    return procedure_cmd_dict, cmd_temp_stats_df, overview_df, mod_cmd_sum_df, module_sum_df

def output_RunResults_excel(segment_to_analyze, overview_df, procedure_cmd_dict, cmd_temp_stats_df, mod_cmd_sum_df, module_sum_df, proc_name_lst, temp_save_df, proc_temp_dfs, xml_errors, proc_er_df):
    fname = "ScriptRunResults_%s.xlsx" % segment_to_analyze
    writer = pd.ExcelWriter(fname, engine='xlsxwriter', datetime_format='hh:mm:ss.000')
    workbook = writer.book
    overview_tab_df = make_run_overview_df(overview_df, proc_name_lst)  #scanning looks good
    sumtime = pd.Timestamp(0)
    overview_tab_df['ImaginaryStartTime'] = [sumtime + np.sum(overview_tab_df['Average Execution Time\n(HH:MM:SS:sss)'].values[:i]) for i in range(len(overview_tab_df['Average Execution Time\n(HH:MM:SS:sss)']))]
    overview_tab_df['Average Execution Time\n(HH:MM:SS:sss)'] = overview_tab_df['Average Execution Time\n(HH:MM:SS:sss)'] + pd.Timestamp(0)
    overview_tab_df['Execution Time Standard Deviation\n(HH:MM:SS:sss)'] = overview_tab_df['Execution Time Standard Deviation\n(HH:MM:SS:sss)'] + pd.Timestamp(0)
    overview_tab_df['Maximum Execution Time\n(HH:MM:SS:sss)'] = overview_tab_df['Maximum Execution Time\n(HH:MM:SS:sss)'] + pd.Timestamp(0)
    overview_tab_df['Minimum Execution Time\n(HH:MM:SS:sss)'] = overview_tab_df['Minimum Execution Time\n(HH:MM:SS:sss)'] + pd.Timestamp(0)
    overview_tab_df.to_excel(writer, sheet_name='Run Overview')
    xml_errors = xml_errors[['Error Type', 'DateTime', 'Cycle', 'Procedure','Message/Returns']]
    xml_errors.to_excel(writer, sheet_name='Run Overview', startcol=1, startrow=32, index=False)
    overview_sheet = writer.sheets['Run Overview']
    overview_sheet.set_column(0, 3, 30)
    overview_sheet.set_column(5, 5, 20)
    overview_sheet.set_column(4, 4, 10)
    overview_sheet.set_column(6, 6, 40)
    header_format = workbook.add_format({
        'bold': True,
        'text_wrap': True,
        'valign': 'top',
        'border': 1
    })
    time_dur_format = workbook.add_format({'num_format': 'hh:mm:ss.000', 'align': 'center'})
    col_save = 0
    for col_num, val in enumerate(overview_tab_df.columns.values):  # rewrite headers in new format
        overview_sheet.write(0, col_num + 1, val, header_format)
        col_save = col_num
    overview_sheet.write(0, col_save+2, 'ImaginaryExecutionTime', header_format)
    save_row = 0
    proc_colors = []
    i=0
    if len(temp_save_df.index) == 0:
        pass
    else:
        temp_save_df = temp_save_df.dropna(axis='columns')
        for h in range(int(len(temp_save_df.columns) / 3)):
            shift_to_imaginary = temp_save_df.iloc[0, (h*3)] - pd.Timestamp(0)
            temp_save_df.iloc[:, (h * 3)] = temp_save_df.iloc[:, (h * 3)] - shift_to_imaginary
    temp_save_df.to_excel(writer, sheet_name = 'Run Overview', index = False, startrow=499)
    for row in range(len(overview_tab_df.index)):
        excel_row = row+1
        save_row = excel_row
        overview_sheet.write(excel_row, col_save + 2, "=H%s-H%s"%(excel_row+2, excel_row+1), time_dur_format)
        proc_colors.append({'fill':{'color': colors[i%len(colors)]}})
        i+=1
    overview_sheet.write(save_row, col_save + 2, "=B%s-H2" % (len(overview_tab_df.index) + 1), time_dur_format)
    ##
    bar_chart = workbook.add_chart({'type': 'bar', 'subtype': 'stacked'})
    bar_chart.add_series({
        'name': ["Run Overview", 0, 7],
        'categories': "=('Run Overview'!$A$2:$A$%s)" % (len(overview_tab_df.index) + 1),  # $N$16
        'values': "=('Run Overview'!$H$2:$H$%s)" % (len(overview_tab_df.index) + 1),
        'fill': {'none': True}
    })
    bar_chart.add_series({
        'name': ["Run Overview", 0, 1],
        'values': "=('Run Overview'!$I$2:$I$%s)" % (len(overview_tab_df.index) + 1),
        'points': proc_colors
    })
    bar_chart.set_x_axis({
        'date_axis': True,
        'min': 25569,
        'num_format': 'hh:mm:ss.000'
    })
    bar_chart.set_size({
        'width': 1300,
        'height': 600
    })
    bar_chart.set_legend({'none': True})
    scatter_chart = workbook.add_chart({'type': 'scatter', 'subtype': 'smooth_with_markers'})
    for p in range(int(len(temp_save_df.columns)/3)):
        date_col_letter = xl_col_to_name((p*3))
        tec_col_letter = xl_col_to_name((p*3)+1)
        sens_col_letter = xl_col_to_name((p*3)+2)
        num_rows = len(temp_save_df.dropna().iloc[:, (p*3)].values)
        scatter_chart.add_series({
            'name': ["Run Overview", 499, (p*3)+1],
            'categories': "='Run Overview'!$%s$501:$%s$%s" % (date_col_letter, date_col_letter, (500 + num_rows)),
            'values': "='Run Overview'!$%s$501:$%s$%s" % (tec_col_letter, tec_col_letter, (500 + num_rows)),
            'y2_axis': True
        })
        scatter_chart.add_series({
            'name': ["Run Overview", 499, (p*3)+2],
            'categories': "='Run Overview'!$%s$501:$%s$%s" % (date_col_letter, date_col_letter, (500 + num_rows)),
            'values': "='Run Overview'!$%s$501:$%s$%s" % (sens_col_letter, sens_col_letter, (500 + num_rows)),
            'y2_axis': True
        })
    combine_chart = bar_chart.combine(scatter_chart)
    scatter_chart.set_y2_axis({'name': 'Temperature (C)'})
    bar_chart.set_legend({'position': 'right'})
    overview_sheet.insert_chart('B1', bar_chart) #moved chart location
    ##
    # overview_df['Execution Time'] = overview_df['Execution Time'] + pd.Timestamp(0) #  THis doesnt work for some reason.  cant apply to full DF?
    for proc in proc_name_lst:
        if len(proc)>=29:
            proc_name = proc[:28]
        else:
            proc_name = proc
        # proc_exec_time_overview =
        proc_cmd_dict = {}
        select_proc = overview_df.loc[overview_df['Procedure'] == proc]
        select_proc = select_proc.drop(['Procedure', 'Execution Time'], axis=1)
        select_proc['Execution Time\n(HH:MM:SS:sss)'] = select_proc['Execution Time\n(HH:MM:SS:sss)'] + pd.Timestamp(0)  # already done?
        select_proc.to_excel(writer, sheet_name=proc_name, index=False)
        proc_sheet = writer.sheets[proc_name]
        proc_sheet.set_column(0, 3, 9)
        proc_sheet.set_column(2, 2, 15)
        for col_num, val in enumerate(select_proc.columns.values):  # rewrite headers in new format
            proc_sheet.write(0, col_num, val, header_format)
        iso_proc = cmd_temp_stats_df.loc[cmd_temp_stats_df['Procedure'] == proc]
        first_unique_cyc = np.amin(np.unique(iso_proc['Cycle'].values))
        cycle_zero_cmds = iso_proc.loc[iso_proc['Cycle'] == first_unique_cyc, 'Command'].values
        ordered_cmds = []
        for cmd in cycle_zero_cmds:
            if cmd in ordered_cmds:
                break
            else:
                ordered_cmds.append(cmd)
        i = 0
        for cmd in ordered_cmds:
            iso_cmd = iso_proc.loc[iso_proc['Command'] == cmd]
            # time dur stats
            time_vals = iso_cmd['Exec. Time'].values.astype(np.int64)
            avg_time = np.mean(time_vals).astype(np.timedelta64)
            max_time = np.amax(time_vals).astype(np.timedelta64) + pd.Timestamp(0)
            min_time = np.amin(time_vals).astype(np.timedelta64) + pd.Timestamp(0)
            arg_max = iso_cmd['Cycle'].values[np.argmax(time_vals)]
            arg_min = iso_cmd['Cycle'].values[np.argmin(time_vals)]
            std_time = np.std(time_vals).astype(np.timedelta64)
            # TEC temp stats
            tec_vals = iso_cmd['TEC_Mean'].dropna().values
            if len(tec_vals) == 0:
                tec_avg = None
                tec_std = None
                tec_max = None
                tec_argmax = None
                tec_min = None
                tec_argmin = None
                tec_max_std = None
                tec_argmax_std = None
            else:
                tec_mean_vals = iso_cmd['TEC_Mean'].values.round(decimals = 3)
                tec_avg = _nanmean(tec_mean_vals).round(decimals = 3)
                tec_std = _nanstd(tec_mean_vals).round(decimals = 3)
                tec_max = _nanmax(tec_mean_vals).round(decimals = 3)
                tec_argmax = iso_cmd['Cycle'].values[_nanargmax(tec_mean_vals)]
                tec_min = _nanmin(tec_mean_vals).round(decimals = 3)
                tec_argmin = iso_cmd['Cycle'].values[_nanargmin(tec_mean_vals)]
                tec_max_std = _nanmax(iso_cmd['TEC_Std'].values).round(decimals = 3)
                tec_argmax_std = iso_cmd['Cycle'].values[_nanargmax(iso_cmd['TEC_Std'].values.round(decimals = 3))]
            # Sensor temp stats
            sensor_vals = iso_cmd['Sensor_Mean'].dropna().values
            if len(sensor_vals) == 0:
                sens_mean_vals = None
                sens_avg = None
                sens_std = None
                sens_max = None
                sens_argmax = None
                sens_min = None
                sens_argmin = None
                sens_max_std = None
                sens_argmax_std = None
            else:
                sens_mean_vals = iso_cmd['Sensor_Mean'].values.round(decimals = 3)
                sens_avg = _nanmean(sens_mean_vals).round(decimals = 3)
                sens_std = _nanstd(sens_mean_vals).round(decimals = 3)
                sens_max = _nanmax(sens_mean_vals).round(decimals = 3)
                sens_argmax = iso_cmd['Cycle'].values[_nanargmax(sens_mean_vals)]
                sens_min = _nanmin(sens_mean_vals).round(decimals = 3)
                sens_argmin = iso_cmd['Cycle'].values[_nanargmin(sens_mean_vals)]
                sens_max_std = _nanmax(iso_cmd['Sensor_Std'].values).round(decimals = 3)
                sens_argmax_std = iso_cmd['Cycle'].values[_nanargmax(iso_cmd['Sensor_Std'].values.round(decimals = 3))]
            proc_cmd_dict[i] = [avg_time, std_time, (max_time.strftime('%H:%M:%S:%f'), arg_max), (min_time.strftime('%H:%M:%S:%f'), arg_min),
                                tec_avg, tec_std, (tec_max, tec_argmax), (tec_min, tec_argmin), (tec_max_std, tec_argmax_std),
                                sens_avg, sens_std, (sens_max, sens_argmax), (sens_min, sens_argmin), (sens_max_std, sens_argmax_std)]
            i += 1
        stat_df_cols = ['Average Execution Time\n(HH:MM:SS:sss)', 'Execution Time Standard Deviation\n(HH:MM:SS:sss)',
                        'Maximum Execution Time\n[(HH:MM:SS:sss, Cycle_Number)]', 'Minimum Execution Time\n[(HH:MM:SS:sss, Cycle_Number)]',
                        'TEC\nAverage Temperature\n(Celsius)', 'TEC\nTemperature Standard Deviation\n(Celsius)',
                        'TEC\nMaximum Temperature\n[(Celsius, Cycle Number)]', 'TEC\nMinimum Temperature\n[(Celsius, Cycle Number)]',
                        'TEC\nCycle with Maximum\nStandard Deviation\n[(Celsius, Cycle Number)]',
                        'Sensor\n\nAverage Temperature\n(Celsius)', 'Sensor\nTemperature Standard Deviation\n(Celsius)',
                        'Sensor\nMaximum Temperature\n[(Celsius, Cycle Number)]', 'Sensor\nMinimum Temperature\n[(Celsius, Cycle Number)]',
                        'Sensor\nCycle with Maximum\nStandard Deviation\n[(Celsius, Cycle Number)]']
        proc_cmd_stat_df = pd.DataFrame.from_dict(proc_cmd_dict, orient='index', columns=stat_df_cols)
        proc_cmd_stat_df['Average Execution Time\n(HH:MM:SS:sss)'] = proc_cmd_stat_df['Average Execution Time\n(HH:MM:SS:sss)'] + pd.Timestamp(0)
        proc_cmd_stat_df['Execution Time Standard Deviation\n(HH:MM:SS:sss)'] = proc_cmd_stat_df['Execution Time Standard Deviation\n(HH:MM:SS:sss)'] + pd.Timestamp(0)
        #proc_cmd_stat_df['Max Exec.T'] = proc_cmd_stat_df['Max Exec.T'][0] + pd.Timestamp(0)
        #proc_cmd_stat_df['Min Exec.T'] = proc_cmd_stat_df['Min Exec.T'][0] + pd.Timestamp(0)
        proc_cmd_df = procedure_cmd_dict[proc]  #edit these index
        save_index = proc_cmd_df.index
        proc_cmd_df = pd.concat([proc_cmd_df.reset_index(drop=True), proc_cmd_stat_df.reset_index(drop=True)], axis=1)
        proc_cmd_df = proc_cmd_df.set_index(save_index)
        fillvals = {'Average Execution Time\n(HH:MM:SS:sss)':pd.Timestamp(0),'Execution Time Standard Deviation\n(HH:MM:SS:sss)':pd.Timestamp(0),
       'Maximum Execution Time\n[(HH:MM:SS:sss, Cycle_Number)]':pd.Timestamp(0),
       'Minimum Execution Time\n[(HH:MM:SS:sss, Cycle_Number)]':pd.Timestamp(0),
       'TEC\nAverage Temperature\n(Celsius)':0,
       'TEC\nTemperature Standard Deviation\n(Celsius)':0,
       'TEC\nMaximum Temperature\n[(Celsius, Cycle Number)]':0,
       'TEC\nMinimum Temperature\n[(Celsius, Cycle Number)]':0,
       'TEC\nCycle with Maximum\nStandard Deviation\n[(Celsius, Cycle Number)]':0,
       'Sensor\n\nAverage Temperature\n(Celsius)':0,
       'Sensor\nTemperature Standard Deviation\n(Celsius)':0,
       'Sensor\nMaximum Temperature\n[(Celsius, Cycle Number)]':0,
       'Sensor\nMinimum Temperature\n[(Celsius, Cycle Number)]':0,
       'Sensor\nCycle with Maximum\nStandard Deviation\n[(Celsius, Cycle Number)]':0}
        proc_cmd_df = proc_cmd_df.fillna(value = fillvals)
        proc_cmd_df.to_excel(writer, sheet_name=proc_name, startcol=4)
        if proc_er_df.empty: proc_relev_err_df = pd.DataFrame(columns=['Error Type', 'DateTime','Cycle', 'Message/Returns'])
        else:
            if proc in proc_er_df['Procedure'].values:
                proc_relev_err_df = proc_er_df.loc[proc_er_df['Procedure'] == proc].drop(columns = 'Procedure')
            else:
                proc_relev_err_df = proc_er_df[0:0].drop(columns = 'Procedure')
        proc_relev_err_df = proc_relev_err_df[['Error Type', 'DateTime','Cycle', 'Message/Returns']]
        proc_relev_err_df.to_excel(writer, sheet_name=proc_name, startcol=5, startrow=len(save_index)+1, index = False)
        for col_num, val in enumerate(proc_cmd_df.columns.values):  # rewrite headers in new format
            proc_sheet.write(0, col_num+5, val, header_format)
        proc_sheet.set_column(4, 6, 20)
        proc_sheet.set_column(7, 7, 30)
        proc_sheet.set_column(8, 9, 15)
        proc_sheet.set_column(10, 11, 20)
        proc_sheet.set_column(12, 25, 20)
        proc_sheet.freeze_panes(0, 8)
        scatter_chart = workbook.add_chart({'type':'scatter', 'subtype':'smooth_with_markers'})
        new_series_idx = np.where(select_proc.loc[:,"Cycle\n(Number)"].values == first_unique_cyc)[0]
        total_cycles = len(select_proc.loc[:,"Cycle\n(Number)"].values)
        series_end_idx = [x-1 for x in new_series_idx[1:]]
        series_end_idx.append(total_cycles-1)
        num_lines = len(new_series_idx)
        start_cellnum = 2
        new_series_idx += start_cellnum
        series_end_idx = [x+start_cellnum for x in series_end_idx]
        for series in range(num_lines):
            start = new_series_idx[series]
            end = series_end_idx[series]
            scatter_chart.add_series({
                'name': proc_name+" Run %s"%series,#Execution time
                'categories': '''='%s'!$D$%s:$D$%s'''%(proc_name, start,end),
                'values': '''='%s'!$C$%s:$C$%s'''%(proc_name, start,end)
            })#execution time
        scatter_chart.set_size({
        'width': 300,
        'height': 300
    })
        proc_sheet.insert_chart('A1', scatter_chart)
        try:
            proc_temp_df = proc_temp_dfs[proc]
        except KeyError:
            continue
        save_row = 0
        #plotting scatter for imaginary temp
        cmd_colors = []
        i = 0
        if len(proc_temp_df.index) ==0:
            pass
        else:
            for h in range(int(len(proc_temp_df.columns) / 3)):
                #print(proc_temp_df.iloc[0, (h * 3)], type(proc_temp_df.iloc[0, (h * 3)]))
                if type(proc_temp_df.iloc[0, (h * 3)]) == pd.Timestamp:
                    shift_to_imaginary = proc_temp_df.iloc[0, (h * 3)] - pd.Timestamp(0)
                    proc_temp_df.iloc[:, (h * 3)] = proc_temp_df.iloc[:, (h * 3)] - shift_to_imaginary
                else:
                    continue
        proc_temp_df.to_excel(writer, sheet_name=proc_name, index = False, startrow=1499)
        sumtime = pd.Timestamp(0)
        proc_cmd_df['Average Execution Time\n(HH:MM:SS:sss)'] = proc_cmd_df['Average Execution Time\n(HH:MM:SS:sss)'] - sumtime
        proc_cmd_df_numrow = len(proc_cmd_df.index)
        #proc_cmd_df = proc_cmd_df.loc[~proc_cmd_df['Module Command Type'].str.contains("Image "), :]
        imaginary_starttime_arr = []
        imaginary_endtime_arr = []
        cmd_time_dict = {}
        group_start_time = None
        group_end_time = None
        main_cmd = False
        for aet in range(proc_cmd_df_numrow):
            id = proc_cmd_df.index[aet].split("_")
            print(id)
            num = id[0]
            mod = id[1]
            if len(id) >2:
                #groupstep
                grp_seg = id[2]
                if grp_seg == 'Main':
                    if main_cmd:#second or other main cmds
                        group_start_time = sumtime
                        imaginary_starttime_arr.append(sumtime)
                        group_end_time = sumtime +proc_cmd_df['Average Execution Time\n(HH:MM:SS:sss)'].values[aet]
                        imaginary_endtime_arr.append(group_end_time)
                        sumtime = group_end_time
                    else:#first main cmd
                        group_start_time = sumtime
                        group_end_time = sumtime +proc_cmd_df['Average Execution Time\n(HH:MM:SS:sss)'].values[aet]
                        imaginary_starttime_arr.append(group_start_time)
                        imaginary_endtime_arr.append(group_end_time)
                        sumtime = group_end_time
                        main_cmd = True
                elif 'aSub' in grp_seg:
                    # async case, group start time tracks async sub start time; group end time is main end, sumtime is main end
                    imaginary_starttime_arr.append(group_start_time)
                    group_start_time += proc_cmd_df['Average Execution Time\n(HH:MM:SS:sss)'].values[aet]
                    imaginary_endtime_arr.append(group_start_time)
                else:
                    #noasync case
                    imaginary_starttime_arr.append(group_start_time)
                    imaginary_endtime_arr.append(group_start_time+proc_cmd_df['Average Execution Time\n(HH:MM:SS:sss)'].values[aet])
            else:
                #normal step?
                imaginary_starttime_arr.append(sumtime)
                sumtime+= proc_cmd_df['Average Execution Time\n(HH:MM:SS:sss)'].values[aet]
                imaginary_endtime_arr.append(sumtime)
        #imaginary_starttime_arr = proc_cmd_df['ImaginaryStartTime'].values
        proc_imaginary_df = pd.DataFrame()
        proc_imaginary_df['Module_Command'] = proc_cmd_df['Module'] + "_" + proc_cmd_df['Module Command Type']
        proc_imaginary_df['ImaginaryStartTime'] = imaginary_starttime_arr
        proc_imaginary_df['ImaginaryEndTime'] = imaginary_endtime_arr
        proc_imaginary_df = proc_imaginary_df.dropna()
        proc_imaginary_df.to_excel(writer, sheet_name=proc_name, index = False, startrow=499, startcol=7)
        proc_sheet.write(499, 10, 'ImaginaryExecutionTime', header_format)
        col_save = 10
        for row in range(len(proc_imaginary_df.index)):
            excel_row = row + 500
            save_row = excel_row
            proc_sheet.write(excel_row, col_save , "=J%s-I%s" % (excel_row +1, excel_row +1), time_dur_format)
            cmd_colors.append({'fill': {'color': colors[i % len(colors)]}})
            i += 1
        #proc_sheet.write(save_row, col_save, "=I%s-I501" % (proc_cmd_df_numrow + 1), time_dur_format)
        ##
        proc_sheet_name = "'" + proc_name + "'"
        bar_chart = workbook.add_chart({'type': 'bar', 'subtype': 'stacked'})
        bar_chart.add_series({
            'name': [proc_name, 499, 8],
            'categories': "=(%s!$H$501:$H$%s)" % (proc_sheet_name,len(proc_imaginary_df.index) + 500),  # $N$16
            'values': "=(%s!$I$501:$I$%s)" % (proc_sheet_name, len(proc_imaginary_df.index) + 500),
            'fill': {'none': True}
        })
        bar_chart.add_series({
            'name': [proc_name, 499, 9],
            'categories': "=(%s!$H$501:$H$%s)" % (proc_sheet_name, len(proc_imaginary_df.index) + 500),
            'values': "=(%s!$K$501:$K$%s)" % (proc_sheet_name, len(proc_imaginary_df.index) + 500),
            'points': proc_colors
        })
        bar_chart.set_x_axis({
            'date_axis': True,
            'min': 25569,
            'num_format': 'hh:mm:ss.000'
        })
        bar_chart.set_size({
            'width': 1300,
            'height': 600
        })
        bar_chart.set_legend({'none': True})

        scatter_chart = workbook.add_chart({'type': 'scatter', 'subtype': 'smooth_with_markers'})
        for p in range(int(len(proc_temp_df.columns) / 3)):
            date_col_letter = xl_col_to_name((p * 3))
            tec_col_letter = xl_col_to_name((p * 3) + 1)
            sens_col_letter = xl_col_to_name((p * 3) + 2)
            num_rows = len(proc_temp_df.iloc[:, (p * 3)].values)
            scatter_chart.add_series({
                'name': [proc_name, 1499, (p * 3) + 1],
                'categories': "=%s!$%s$1501:$%s$%s" % (proc_sheet_name,date_col_letter, date_col_letter, (1500 + num_rows)),
                'values': "=%s!$%s$1501:$%s$%s" % (proc_sheet_name,tec_col_letter, tec_col_letter, (1500 + num_rows)),
                'y2_axis': True
            })
            scatter_chart.add_series({
                'name': [proc_name, 1499, (p * 3) + 2],
                'categories': "=%s!$%s$1501:$%s$%s" % (proc_sheet_name,date_col_letter, date_col_letter, (1500 + num_rows)),
                'values': "=%s!$%s$1501:$%s$%s" % (proc_sheet_name,sens_col_letter, sens_col_letter, (1500 + num_rows)),
                'y2_axis': True
            })
        combine_chart = bar_chart.combine(scatter_chart)
        scatter_chart.set_y2_axis({'name': 'Temperature (C)'})
        bar_chart.set_legend({'position': 'right'})
        proc_sheet.insert_chart('A20', bar_chart)
    modcmdstatsdf = make_modcmdstatsdf(mod_cmd_sum_df)
    modcmdstatsdf.to_excel(writer, sheet_name= 'Module Command Summary')
    modcmdsum_sheet = writer.sheets['Module Command Summary']
    for i in range(len(modcmdstatsdf.index)):
        td_usec = timedelta(microseconds=int(modcmdstatsdf['Average Execution Time\n(HH:MM:SS.000)'].values[i]))
        modcmdsum_sheet.write_datetime(i+1, 4, td_usec, time_dur_format)
        td_usec = timedelta(microseconds=int(modcmdstatsdf['Execution Time Standard Deviation\n(HH:MM:SS.000)'].values[i]))
        modcmdsum_sheet.write_datetime(i + 1, 5, td_usec, time_dur_format)
        td_usec = timedelta(microseconds=int(modcmdstatsdf['Maximum Execution Time\n(HH:MM:SS.000)'].values[i]))
        modcmdsum_sheet.write_datetime(i + 1, 6, td_usec, time_dur_format)
        td_usec = timedelta(microseconds=int(modcmdstatsdf['Minimum Execution Time\n(HH:MM:SS.000)'].values[i]))
        modcmdsum_sheet.write_datetime(i + 1, 8, td_usec, time_dur_format)
    for col_num, val in enumerate(modcmdstatsdf.columns.values):  # rewrite headers in new format
        modcmdsum_sheet.write(0, col_num + 1, val, header_format)
    modcmdsum_sheet.set_column(1,3, 20)
    modcmdsum_sheet.set_column(4, 6, 25)
    modcmdsum_sheet.set_column(8, 8, 25)
    modsum_stat_df = make_modstatdf(module_sum_df)
    modsum_stat_df.insert(loc=0, column='Module_Command',
                                  value=modsum_stat_df['Module'] + "_" + modsum_stat_df['Module Command Type'])
    modsum_stat_df = modsum_stat_df.sort_values('Average Execution Time\n(HH:MM:SS.000)', ascending = False)
    percentage_arr = modsum_stat_df['Average Execution Time\n(HH:MM:SS.000)'].values / np.sum(modsum_stat_df['Average Execution Time\n(HH:MM:SS.000)'].values)
    cum_sum = 0
    cum_percentage_arr = []
    for x in percentage_arr:
        cum_sum += x
        cum_percentage_arr.append(cum_sum)
    modsum_stat_df['Time Percentage'] = cum_percentage_arr
    modsum_stat_df.to_excel(writer, sheet_name='Module Command Summary', startcol=10, index = False)
    for i in range(len(modsum_stat_df.index)):
        td_usec = timedelta(microseconds=int(modsum_stat_df['Average Execution Time\n(HH:MM:SS.000)'].values[i]))
        modcmdsum_sheet.write_datetime(i+1, 13, td_usec, time_dur_format)
        td_usec = timedelta(microseconds=int(modsum_stat_df['Execution Time Standard Deviation\n(HH:MM:SS.000)'].values[i]))
        modcmdsum_sheet.write_datetime(i + 1, 14, td_usec, time_dur_format)
        td_usec = timedelta(microseconds=int(modsum_stat_df['Maximum Execution Time\n(HH:MM:SS.000)'].values[i]))
        modcmdsum_sheet.write_datetime(i + 1, 15, td_usec, time_dur_format)
        td_usec = timedelta(microseconds=int(modsum_stat_df['Minimum Execution Time\n(HH:MM:SS.000)'].values[i]))
        modcmdsum_sheet.write_datetime(i + 1, 17, td_usec, time_dur_format)
    for col_num, val in enumerate(modsum_stat_df.columns.values):  # rewrite headers in new format
        modcmdsum_sheet.write(0, col_num + 10, val, header_format)
    modcmdsum_sheet.set_column(11,12, 20)
    modcmdsum_sheet.set_column(13, 15, 25)
    modcmdsum_sheet.set_column(17, 17, 25)
    pareto_chart = workbook.add_chart({'type': 'column'})
    pareto_chart.add_series({
        'name': 'Time Elapsed (s)',
        'categories': "='Module Command Summary'!$K$2:$K$%s" % (len(modsum_stat_df.index) + 1),
        'values': "='Module Command Summary'!$N$2:$N$%s" % (len(modsum_stat_df.index) + 1),
        'y_error_bars': {
            'type': 'custom',
            'plus_values': "='Module Command Summary'!$O$2:$O$%s" % (len(modsum_stat_df.index) + 1),
            'minus_values': "='Module Command Summary'!$O$2:$O$%s" % (len(modsum_stat_df.index) + 1)
        }
    })
    pareto_chart.set_legend({'position': 'none'})
    pareto_chart.set_y2_axis({'max': 1.0})
    line_chart = workbook.add_chart({'type': 'line'})
    line_chart.add_series({
        'categories': "='Module Command Summary'!$K$2:$K$%s" % (len(modsum_stat_df.index) + 1),
        'values': "='Module Command Summary'!$T$2:$T$%s" % (len(modsum_stat_df.index) + 1),
        'marker': {'type': 'automatic'},
        'y2_axis': 1
    })
    line_chart.set_y_axis({'max':1.0})
    pareto_chart.combine(line_chart)
    pareto_chart.set_y2_axis({'max': 1.0})
    modcmdsum_sheet.insert_chart('L10', pareto_chart)
    workbook.worksheets_objs.insert(1,  workbook.worksheets_objs.pop(-1))
    try:
        writer.save()
    except Exception as e:
        print("Could Not Output XLSX file.  File of Same Name is Open.")

def make_run_overview_df(overview_df, proc_name_lst):
    overview_tab_df = {}
    for proc in proc_name_lst:#np.unique(overview_df['Procedure']):
        iso_procedure = overview_df.loc[overview_df['Procedure'] == proc]
        iso_procedure = iso_procedure.drop(['Procedure', 'Number of Errors', 'Number of Commands'], axis = 1)
        #iso_procedure
        execution_time = iso_procedure['Execution Time\n(HH:MM:SS:sss)'].values.astype(np.int64)
        average = np.mean(execution_time).astype(np.timedelta64)
        stddev = np.std(execution_time).astype(np.timedelta64)
        amax = np.amax(execution_time).astype(np.timedelta64)
        arg_max_cyc = iso_procedure['Cycle\n(Number)'].values[np.argmax(execution_time)]
        amin = np.amin(execution_time).astype(np.timedelta64)
        arg_min_cyc = iso_procedure['Cycle\n(Number)'].values[np.argmin(execution_time)]
        overview_tab_df[proc] = [average, stddev, amax, arg_max_cyc, amin, arg_min_cyc]
    overview_tab_cols = ['Average Execution Time\n(HH:MM:SS:sss)',
                         'Execution Time Standard Deviation\n(HH:MM:SS:sss)',
                         'Maximum Execution Time\n(HH:MM:SS:sss)',
                         'Cycle with Maximum Time\n(Cycle Number)',
                         'Minimum Execution Time\n(HH:MM:SS:sss)',
                         'Cycle with Minimum Time\n(Cycle Number)']
    return pd.DataFrame.from_dict(overview_tab_df, orient='index', columns = overview_tab_cols)

def _nanmean(arr):
    try:
        return np.nanmean(arr)
    except ValueError:
        return np.nan

def _nanstd(arr):
    try:
        return np.nanstd(arr)
    except ValueError:
        return np.nan

def _nanmax(arr):
    try:
        return np.nanmax(arr)
    except ValueError:
        return np.nan

def _nanargmax(arr):
    try:
        return np.nanargmax(arr)
    except ValueError:
        return None

def _nanmin(arr):
    try:
        return np.nanmin(arr)
    except ValueError:
        return np.nan

def _nanargmin(arr):
    try:
        return np.nanargmin(arr)
    except ValueError:
        return None

def make_modcmd_sum_by_proc(select_cycle):
    proced_modcmd_sum_dict = {}
    i = 0
    for proc in np.unique(select_cycle['Procedure']):
        select_proc = select_cycle.loc[select_cycle['Procedure'] == proc, :]
        for mod in np.unique(select_proc['Module']):
            select_module = select_proc.loc[select_proc['Module'] == mod, :]
            for cmd_type in np.unique(select_module['Module Command Type']):
                select_modcmd = select_module.loc[select_module['Module Command Type'] == cmd_type, :].copy()
                select_modcmd.loc[:, 'Execution Time'] = select_modcmd.loc[:, 'End Time'] - select_modcmd.loc[:, 'Start Time']
                tot_elapsed_time = int(np.sum(select_modcmd['Execution Time'].values))
                proced_modcmd_sum_dict[i] = [proc, mod, cmd_type, tot_elapsed_time]
                i+=1
    modcmd_sum_by_proc_df = pd.DataFrame.from_dict(proced_modcmd_sum_dict,orient='index', columns=['Procedure', 'Module', 'Command Type', 'Elapsed Time'])
    #print(modcmd_sum_by_proc_df['Elapsed Time'])
    return modcmd_sum_by_proc_df

def make_cycle_modcmd_overview(modcmd_sum_by_proc_df):
    cycle_overview_modcmd_dict = {}
    i=0
    #modcmd_sum_by_proc_df = modcmd_sum_by_proc_df['Elapsed Time']
    #print(modcmd_sum_by_proc_df['Elapsed Time'])
    modcmd_sum_by_proc_df = modcmd_sum_by_proc_df.drop('Procedure', axis =1)
    for mod in np.unique(modcmd_sum_by_proc_df['Module']):
        select_module = modcmd_sum_by_proc_df.loc[modcmd_sum_by_proc_df['Module'] == mod, :]
        for cmd_type in np.unique(select_module['Command Type']):
            select_modcmd = select_module.loc[select_module['Command Type'] == cmd_type, :]
            #print(type(select_modcmd['Elapsed Time'].values[0]))
            modcmd_sumtime = np.sum(select_modcmd['Elapsed Time'].values)
            #print(type(modcmd_sumtime), modcmd_sumtime)
            cycle_overview_modcmd_dict[i] = [mod, cmd_type, modcmd_sumtime]
            i+=1
    cyc_modcmd_overview_df = pd.DataFrame.from_dict(cycle_overview_modcmd_dict, orient='index', columns = ['Modules', 'Command Types', 'Total Time'])
    return cyc_modcmd_overview_df

def make_modcmdstatsdf(mod_cmd_sum_df):
    """Summarizes and basic stats on mod_cmd_sum_df, ouputs time as int(microseconds)"""
    modcmd_stats_dict = {}
    mcs = 0
    for proc in np.unique(mod_cmd_sum_df['Procedure'].values):
        select_proc = mod_cmd_sum_df.loc[mod_cmd_sum_df['Procedure'] == proc, :]
        for mod in np.unique(select_proc['Module'].values):
            select_mod = select_proc.loc[select_proc['Module'] == mod, :]
            for cmd_type in np.unique(select_mod['Command Type'].values):
                select_cmd = select_mod.loc[select_mod['Command Type'] == cmd_type, :].copy()
                select_cmd['Elapsed Time'] = select_cmd['Elapsed Time'].map(lambda x: (x.value/1000))
                etime_vals = select_cmd['Elapsed Time'].values
                ave = int(np.mean(etime_vals))
                std = int(np.std(etime_vals))
                max_t = int(np.amax(etime_vals))
                min_t = int(np.amin(etime_vals))
                max_cyc = select_cmd['Cycle'].values[np.argmax(etime_vals)]
                min_cyc = select_cmd['Cycle'].values[np.argmin(etime_vals)]
                modcmd_stats_dict[mcs] = [proc, mod, cmd_type, ave, std, max_t, max_cyc, min_t, min_cyc]
                mcs += 1
    mod_cmd_stats_df = pd.DataFrame.from_dict(modcmd_stats_dict, orient='index', columns=['Procedure', 'Module',
                                                                                          'Module Command Type',
                                                                                          'Average Execution Time\n(HH:MM:SS.000)',
                                                                                          'Execution Time Standard Deviation\n(HH:MM:SS.000)',
                                                                                          'Maximum Execution Time\n(HH:MM:SS.000)',
                                                                                          "Maximum Cycle",
                                                                                          'Minimum Execution Time\n(HH:MM:SS.000)',
                                                                                          "Minimum Cycle"])
    return mod_cmd_stats_df

def datetimeTime_to_timedelta(obj):
    hours = obj._h
    mins = obj._m
    sec = obj._s
    ms = obj._microseconds

    mins = hours * 60 + mins
    sec = mins * 60 + sec
    ms = 1000000 * sec + ms
    return ms

def make_modstatdf(mod_sum_df):
    mod_stat_dict = {}
    mcs = 0
    for mod in np.unique(mod_sum_df['Modules'].values):
        select_mod = mod_sum_df.loc[mod_sum_df['Modules'] == mod, :]
        for cmd_type in np.unique(select_mod['Command Types'].values):
            select_cmd = select_mod.loc[select_mod['Command Types'] == cmd_type, :].copy()
            select_cmd['Total Time'] = select_cmd['Total Time'].map(lambda x: (x.value/1000))
            etime_vals = select_cmd['Total Time'].values
            ave = int(np.mean(etime_vals))
            std = int(np.std(etime_vals))
            max_t = int(np.amax(etime_vals))
            min_t = int(np.amin(etime_vals))
            max_cyc = select_cmd['Cycle'].values[np.argmax(etime_vals)]
            min_cyc = select_cmd['Cycle'].values[np.argmin(etime_vals)]
            mod_stat_dict[mcs] = [mod, cmd_type, ave, std, max_t, max_cyc, min_t, min_cyc]
            mcs += 1
    mod_stat_df = pd.DataFrame.from_dict(mod_stat_dict, orient='index', columns=['Module',
                                                                                          'Module Command Type',
                                                                                          'Average Execution Time\n(HH:MM:SS.000)',
                                                                                          'Execution Time Standard Deviation\n(HH:MM:SS.000)',
                                                                                          'Maximum Execution Time\n(HH:MM:SS.000)',
                                                                                          "Maximum Cycle",
                                                                                          'Minimum Execution Time\n(HH:MM:SS.000)',
                                                                                          "Minimum Cycle"])
    return mod_stat_df

def output_grp_modcmd_sheet(grp_cmd_df, writer, workbook):
    header_format = workbook.add_format({
        'bold': True,
        'text_wrap': True,
        'valign': 'top',
        'align': 'center',
        'border': 1
    })
    time_dur_format = workbook.add_format({'num_format': 'hh:mm:ss.000', 'align': 'center'})
    modcmd_sum_byproc_df = make_modcmd_sum_by_proc(grp_cmd_df)
    cyc_modcmd_overview_df = make_cycle_modcmd_overview(modcmd_sum_byproc_df)
    cyc_modcmd_overview_df.insert(loc=0, column='Module_Command',
                                  value=cyc_modcmd_overview_df['Modules'] + "_" + cyc_modcmd_overview_df[
                                      'Command Types'])
    percentage_arr = cyc_modcmd_overview_df['Total Time'].values / np.sum(cyc_modcmd_overview_df['Total Time'].values)
    cum_sum = 0
    cum_percentage_arr = []
    for x in percentage_arr:
        cum_sum += x
        cum_percentage_arr.append(cum_sum)
    cyc_modcmd_overview_df['Time Percentage'] = cum_percentage_arr
    # modcmd_sum_byproc_df['Elapsed Time'] = modcmd_sum_byproc_df['Elapsed Time'].map(lambda x: timedelta(microseconds=(x/1000)))
    modcmd_sum_byproc_df.to_excel(writer, sheet_name="Module Command Summary")
    cyc_modcmd_overview_df.to_excel(writer, sheet_name="Module Command Summary", index=False, startcol=5)
    mod_cmd_sum_sheet = writer.sheets["Module Command Summary"]
    mod_cmd_sum_sheet.set_column(1, 3, 20)
    mod_cmd_sum_sheet.set_column(5, 7, 20)
    mod_cmd_sum_sheet.set_row(0, None, header_format)
    mod_cmd_sum_sheet.set_column(4, 4, 25)
    for i in range(len(modcmd_sum_byproc_df.index)):
        td_usec = timedelta(microseconds=modcmd_sum_byproc_df['Elapsed Time'].values[i] / 1000)
        mod_cmd_sum_sheet.write_datetime(i + 1, 4, td_usec, time_dur_format)
    for i in range(len(cyc_modcmd_overview_df.index)):
        td_usec = timedelta(microseconds=cyc_modcmd_overview_df['Total Time'].values[i] / 1000)
        mod_cmd_sum_sheet.write_datetime(i + 1, 8, td_usec, time_dur_format)
    mod_cmd_sum_sheet.set_column(8, 8, 25)  # , time_dur_format)
    pareto_chart = workbook.add_chart({'type': 'column'})
    pareto_chart.add_series({
        'name': 'Time Elapsed (s)',
        'categories': "='Module Command Summary'!$F$2:$F$%s" % (len(cyc_modcmd_overview_df.index) + 1),
        'values': "='Module Command Summary'!$I$2:$I$%s" % (len(cyc_modcmd_overview_df.index) + 1)
    })
    pareto_chart.set_legend({'position': 'none'})
    line_chart = workbook.add_chart({'type': 'line'})
    line_chart.add_series({
        'categories': "='Module Command Summary'!$F$2:$F$%s" % (len(cyc_modcmd_overview_df.index) + 1),
        'values': "='Module Command Summary'!$J$2:$J$%s" % (len(cyc_modcmd_overview_df.index) + 1),
        'marker': {'type': 'automatic'},
        'y2_axis': 1
    })
    pareto_chart.combine(line_chart)
    pareto_chart.set_y2_axis({'max': 1})
    mod_cmd_sum_sheet.insert_chart('F10', pareto_chart)
    return writer, workbook

def close_data_forOutput(overvw_df, procerror_df, relevxml_errors):
    """Compiles and closes dataframes, removing or clearing None values for missing time/temp.  Reindex error dfs"""
    overvw_df = overvw_df.dropna(axis='index')
    procerror_df = procerror_df.dropna(axis='index').reset_index()
    procerror_df = procerror_df.drop(columns = 'index')
    if relevxml_errors.empty:
        relevxml_errors= pd.DataFrame(columns = ['Error Type', 'DateTime', 'Cycle', 'Procedure','Message/Returns'])
    relevxml_errors = relevxml_errors.dropna(axis='index').reset_index()
    relevxml_errors = relevxml_errors.drop(columns='index')
    return overvw_df, procerror_df, relevxml_errors

def compile_from_closedscripts_queue(xml_runs, step, out_queue):
    #find total script cycles
    tot_script_cyc = 0
    for xmlrun in xml_runs:
        tot_script_cyc += len(xmlrun.script_dict.items())
    que_status_track = 0
    out_queue.put(
        ['Compiling Script Cycle %s of %s' % (que_status_track, tot_script_cyc), (que_status_track, tot_script_cyc)],
        block=False)
    #compile dfs/dicts
    cycle_type_data_dict = {}
    cycle_type_dict = {}
    if type(step) is not int:
        try:
            step = int(step)
        except TypeError:
            step = 20
    total_scripts = 0
    xml_errors = []
    for xml_num, xmlr in zip(range(len(xml_runs)), xml_runs):
        print("XML Num: %s" % xml_num)
        # print(xmlr.script_dict)
        xml_error_df = xmlr.check_errors()
        for scriptnum, script_tup in xmlr.script_dict.items():
            print("ScriptNum: %s" % scriptnum, script_tup[0])
            print(script_tup)
        #print(first_cycle, last_cycle, num_cycles, step)
            print("Compiling Output Closed XML %s, ScriptCycle %s" %(xml_num,scriptnum))
            print(script_tup[1].cycle)
            print([proc.name for proc in script_tup[1].procedures])
            cycle_type, overview_proc_df = script_tup[1].script_return_df(scriptnum)  #looks clear of glaring errors
            if cycle_type == 'Manual_Default_Script':
                continue
            if cycle_type in cycle_type_data_dict:
                overview_df = cycle_type_data_dict[cycle_type][0]
                mod_cmd_sum_df = cycle_type_data_dict[cycle_type][1]
                module_sum_df = cycle_type_data_dict[cycle_type][2]
                procedure_cmd_dict = cycle_type_data_dict[cycle_type][3]
                command_temp_stats_dict = cycle_type_data_dict[cycle_type][4]
                c = cycle_type_data_dict[cycle_type][5]
                types_cycle = cycle_type_data_dict[cycle_type][7]
                types_cycle += 1
                temp_save_df = cycle_type_data_dict[cycle_type][8]
                proc_temp_dfs = cycle_type_data_dict[cycle_type][9]
                procedure_error_df = cycle_type_data_dict[cycle_type][10]
            else:
                # [overviewdf, mod_cmd_sum_df, module_sum_df, procedure_cmd_dict, command_temp_stats_dict]
                overview_df = pd.DataFrame()
                mod_cmd_sum_df = pd.DataFrame()
                module_sum_df = pd.DataFrame()
                procedure_cmd_dict = {}
                command_temp_stats_dict = {}
                c = 0
                types_cycle = 0
                temp_save_df = pd.DataFrame()
                proc_temp_dfs = {}
                procedure_error_df = pd.DataFrame(columns=['Error Type', 'DateTime', 'Message/Returns', 'Cycle'])
            overview_df = pd.concat([overview_df, overview_proc_df], ignore_index=True)
            names = []
            rep = 1
            cycle = script_tup[1].cycle
            cycle_type_dict[total_scripts] = [xml_num,cycle, cycle_type]
            total_scripts += 1
            temp_df = script_tup[1].temp_record_df
            grp_cmd_df = pd.DataFrame()
            if types_cycle%step == 0:
                temp = temp_df.dropna()
                temp.columns = [str(col)+'_'+str(scriptnum) for col in temp.columns]
                temp_save_df = pd.concat([temp_save_df, temp.reset_index(drop=True)], axis=1)
                print("Cycle temp saved as: %s "%scriptnum)

            for proc in script_tup[1].procedures:
                if proc.name is None:
                    print("Procedure Name is None.  This should not happen.")
                # check repeat proc names!
                if proc.name in names:
                    if len(proc.name) >= 31:
                        proc.name = proc.name[:28] + "_%s" % rep
                        rep += 1
                    else:
                        proc.name = proc.name + "_%s" % rep
                        rep += 1
                    names.append(proc.name)
                else:
                    names.append(proc.name)
                proc_df, proc_grp_cmdmod_df, trimmed_temp_df, error_df = proc.proc_return_df(temp_df)  #looks clear of glaring errors
                if not error_df.empty:
                    error_df.loc[:, 'Cycle'] = cycle
                    error_df.loc[:, 'Procedure'] = proc.name
                    procedure_error_df = pd.concat([procedure_error_df, error_df], axis=0)
                    xml_error_df = pd.concat([xml_error_df, error_df], axis=0)
                grp_cmd_df = pd.concat([grp_cmd_df, proc_grp_cmdmod_df], axis=0)
                command_info = proc_df.iloc[:, :3]
                if proc.name not in procedure_cmd_dict:
                    command_info.loc[:, 'Command_in_Cycle'] = 1
                    procedure_cmd_dict[proc.name] = command_info
                    temp_data_df = trimmed_temp_df.loc[trimmed_temp_df['Sensor Temp'] != 0, :]
                    temp_data_df = temp_data_df.dropna()
                else:
                    proc_cmd_dictdf = procedure_cmd_dict[proc.name]
                    proc_cmd_df = proc_cmd_dictdf.loc[:, proc_cmd_dictdf.columns != 'Command_in_Cycle']
                    if proc_cmd_df.equals(command_info):
                        procedure_cmd_dict[proc.name]['Command_in_Cycle'] +=1
                        temp_data_df = trimmed_temp_df.loc[trimmed_temp_df['Sensor Temp'] != 0, :]
                        temp_data_df = temp_data_df.dropna()
                    else:
                        cmd_info_idx = len(command_info.index.values)
                        proc_cmd_df_idx = len(proc_cmd_df.index.values)
                        cic = procedure_cmd_dict[proc.name]['Command_in_Cycle'].values
                        #print(cic)
                        if cmd_info_idx > proc_cmd_df_idx:
                            compare_result = command_info.eq(proc_cmd_df)
                            false = compare_result.loc[compare_result['Module'] == False, :].index
                            tru = compare_result.loc[compare_result['Module'] == True, :].index
                            #print(false)
                            diff_row = command_info.loc[command_info.index.isin(false),:]
                            print(diff_row)
                            diff_row  = diff_row.assign(Command_in_Cycle = 1)
                            proc_cmd_dictdf = pd.concat([proc_cmd_dictdf, diff_row])
                        else:
                            compare_result = proc_cmd_df.eq(command_info)
                            #false = compare_result.loc[compare_result['Module'] == False, :].index
                            tru = compare_result.loc[compare_result['Module'] == True, :].index
                        proc_cmd_dictdf.loc[proc_cmd_dictdf.index.isin(tru), 'Command_in_Cycle'] += 1
                        procedure_cmd_dict[proc.name] = proc_cmd_dictdf
                        temp_data_df = trimmed_temp_df.loc[trimmed_temp_df['Sensor Temp'] != 0, :]
                        temp_data_df = temp_data_df.dropna()
                if types_cycle%step == 0:
                    temp = temp_data_df.dropna()  #start save proc temp data
                    temp.columns = [str(col) + '_' + str(scriptnum) for col in temp.columns]   #append cycle number to columns
                    try:
                        prev_df = proc_temp_dfs[proc.name]  #try extract previous saved proc df
                    except KeyError:
                        prev_df = pd.DataFrame()   #none exists, make one
                    temp = pd.concat([prev_df, temp.reset_index(drop=True)], axis =1)  #concat new data
                    proc_temp_dfs[proc.name] = temp
                for cmd_idx, start, end in zip(command_info.index.values, proc_df['Start Time'].values, proc_df['End Time'].values):
                    if len(temp_data_df.index) == 0:
                        command_temp_stats_dict[c] = [scriptnum, proc.name, cmd_idx, end - start,
                                                      None, None, None, None,
                                                      None, None, None, None]
                    else:
                        temp_segment = temp_data_df.loc[temp_data_df['DateTime'].values >= start]
                        temp_segment = temp_segment.loc[temp_segment['DateTime'].values <= end]
                        if len(temp_segment.index) > 0:
                            tec = temp_segment['TEC Temp'].values
                            sensor = temp_segment['Sensor Temp'].values
                            tec_mean = np.mean(tec)
                            tec_std = np.std(tec)
                            tec_max = np.amax(tec)
                            tec_min = np.amin(tec)
                            sens_mean = np.mean(sensor)
                            sens_std = np.std(sensor)
                            sens_max = np.amax(sensor)
                            sens_min = np.amin(sensor)
                            command_temp_stats_dict[c] = [scriptnum, proc.name, cmd_idx, end - start,
                                                          tec_mean, tec_std, tec_max, tec_min,
                                                          sens_mean, sens_std, sens_max, sens_min]
                        else:
                            command_temp_stats_dict[c] = [scriptnum, proc.name, cmd_idx, end - start,
                                                          None, None, None, None,
                                                          None, None, None, None]
                    c += 1
            #print(grp_cmd_df, i)
            modcmd_sum_byproc_df = make_modcmd_sum_by_proc(grp_cmd_df)#  summarized module_cmd type(cmos_image) by procedure
            cyc_modcmd_overview_df = make_cycle_modcmd_overview(modcmd_sum_byproc_df) #summarized module_cmd type(cmos_image) by cycle
            modcmd_sum_byproc_df['Elapsed Time'] = [timedelta(microseconds= 1000) if x is None else timedelta(microseconds= x / 1000) for x in modcmd_sum_byproc_df['Elapsed Time'].values]  #script summarize elapsed time total
            modcmd_sum_byproc_df['Cycle'] = scriptnum
            mod_cmd_sum_df = pd.concat([mod_cmd_sum_df, modcmd_sum_byproc_df])
            cyc_modcmd_overview_df['Total Time'] = [timedelta(microseconds= 1000) if x is None else timedelta(microseconds= x / 1000) for x in cyc_modcmd_overview_df['Total Time'].values]
            cyc_modcmd_overview_df['Cycle'] = scriptnum
            module_sum_df = pd.concat([module_sum_df, cyc_modcmd_overview_df])
            #print(grp_cmd_df, i, modcmd_sum_byproc_df)
            #print(i,cyc_modcmd_overview_df, modcmd_sum_byproc_df)
            cycle_type_data_dict[cycle_type] = [overview_df, mod_cmd_sum_df, module_sum_df, procedure_cmd_dict, command_temp_stats_dict, c, names, types_cycle, temp_save_df, proc_temp_dfs, procedure_error_df]
            que_status_track += 1
            out_queue.put(['Compiling Script Cycle %s of %s'%(que_status_track, tot_script_cyc), (que_status_track, tot_script_cyc)], block=False)
        xml_errors.append(xml_error_df)
    out_queue.put(
        ['Finishing Compile', 95],
        block=False)
    for cycle_type in cycle_type_data_dict:
        cycle_type_data_dict[cycle_type][4] = pd.DataFrame.from_dict(cycle_type_data_dict[cycle_type][4], orient='index',columns=['Cycle', 'Procedure', 'Command', 'Exec. Time',
                                                        'TEC_Mean', 'TEC_Std', 'TEC_Max', 'TEC_Min',
                                                        'Sensor_Mean', 'Sensor_Std', 'Sensor_Max', 'Sensor_Min'])
    cycle_type_df = pd.DataFrame.from_dict(cycle_type_dict, orient='index', columns=['XML Runs','Run Cycle', 'Cycle Type'])
    cycle_type_df.index.name = 'Cycle Order'
    return cycle_type_df, cycle_type_data_dict, xml_errors

def main_run(log, temps, autotemp, out_que, step):
    if autotemp:
        temps = None
    out_que.put(["Reading Log File", 10], block=False)
    xml_runs = analyze_logfile(log, temps)
    out_que.put(['Reading Complete, Compiling...', 20], block=False)
    cycletypedf, cycletypedatadict, xmlerrordf = compile_from_closedscripts_queue(xml_runs, step, out_que)
    return cycletypedf, cycletypedatadict, xmlerrordf, xml_runs

def output_by_cycle_que(xml_runs, segment, log, disp_queue):

    output_files = []
    disp_queue.put(['Preping Output', 10], block=False)
    #count cycles to output:
    tot_cyc = 0
    for xml_num, xmlr in zip(range(len(xml_runs)), xml_runs):
        num_cyc = len(xmlr.script_dict.items())
        tot_cyc += num_cyc
    que_stat_track = 0
    disp_queue.put(['Output %s ScriptCycle Files'%tot_cyc, 20], block=False)
    for xml_num, xmlr in zip(range(len(xml_runs)), xml_runs):
        print("XML Num: %s" % xml_num)
        for scriptnum, script_tup in xmlr.script_dict.items():
            print("ScriptNum: %s, Segment: %s" % (scriptnum, script_tup[0]))
            disp_queue.put(['Output Script Cycle %s of %s'%(que_stat_track, tot_cyc), (que_stat_track, tot_cyc)], block=False)
            rm_suff = script_tup[0][:-4]
            seg_compare = "_".join(script_tup[0].split(" "))
            if segment in seg_compare:
                writer, workbook, fname = script_tup[1].script_output_df(scriptnum, xml_num)
                names = []
                rep = 1
                temp_df = script_tup[1].temp_record_df
                grp_cmd_df = pd.DataFrame()
                for proc in script_tup[1].procedures:
                    # check repeat proc names!
                    if proc.name in names:
                        if len(proc.name) >= 31:
                            proc.name = proc.name[:28] + "_%s" % rep
                            rep += 1
                        else:
                            proc.name = proc.name + "_%s" % rep
                            rep += 1
                        names.append(proc.name)
                    else:
                        names.append(proc.name)
                    writer, workbook, proc_grp_cmdmod_df = proc.proc_output_df(writer, workbook, temp_df)
                    grp_cmd_df = pd.concat([grp_cmd_df, proc_grp_cmdmod_df], axis=0)
                writer, workbook = output_grp_modcmd_sheet(grp_cmd_df, writer, workbook)
                writer.save()
                output_files.append(fname)
                que_stat_track +=1
    disp_queue.put(['Cleaning Up Files From Directory', 95], block = False)
    output_cleanup(output_files, log)



if __name__ == "__main__":
    logs = all_files(".log")
    for count, ele in list(enumerate(logs)):
        print(count, "\t", ele)
    log_num = int(input("Enter Log Value:  "))
    log = logs[log_num]
    temps = all_files(".csv")
    print(temps)
    t0 = time.perf_counter()
    #closed_scripts = analyze_logfile(log, temps[0])
    xml_runs = analyze_logfile(log, None)
    t1 = time.perf_counter()
    #output_all_closed(closed_scripts, log)
    #cycle_lst = [0,1,2,3,4,5,6,7,8,9,50]
    #output_by_cycle(cycle_lst, closed_scripts, log)
    cycle_type_df, cycle_type_data_dict, xml_errors = compile_from_closedscripts(xml_runs, 20)  #clear here
    cycle_type_df.to_html('CycleType_df.html')
    t2 = time.perf_counter()
    value = 0
    while value != 99:
        for count, ele in list(enumerate(cycle_type_data_dict.keys())):
            print(count, "\t", ele)
        print("99\tExit")
        value = input("Enter Segment Value:  ")
        value = int(value)
        if value == 99:
           continue
        t4 = time.perf_counter()
        print(list(cycle_type_data_dict.keys())[value])
        seg = list(cycle_type_data_dict.keys())[value]
        relev_xml = cycle_type_df.loc[cycle_type_df['Cycle Type'] == seg, :]
        uniqe_xml_num = np.unique(relev_xml['XML Runs'].values)
        relev_xml_errors = pd.DataFrame()
        for val in uniqe_xml_num:
            relev_xml_errors = pd.concat([relev_xml_errors, xml_errors[val]], axis =0)
        relev_xml_errors.sort_values(by=['DateTime'], inplace=True, ascending=True)
        overview_df, mod_cmd_sum_df, module_sum_df, procedure_cmd_dict, cmd_temp_stats_df, num_cmds, proc_name_lst, types_cycle, temp_save_df,proc_temp_dfs, proc_error_df = cycle_type_data_dict[list(cycle_type_data_dict.keys())[value]]
        #check nan time:  overview_df
        #reset index on proc_error_df, relev_xml_errors
        overview_df, proc_error_df, relev_xml_errors = close_data_forOutput(overview_df, proc_error_df, relev_xml_errors)
        output_RunResults_excel(value, overview_df, procedure_cmd_dict,
                                cmd_temp_stats_df, mod_cmd_sum_df, module_sum_df, proc_name_lst, temp_save_df,
                                proc_temp_dfs, relev_xml_errors, proc_error_df)
        t3 = time.perf_counter()
        print("t0:  %s\nt1:  %s\nt2:  %s" % ((t1 - t0), (t2 - t1), (t3 - t4)))
        val = input("Y to output individual Cycles (Many Files, Takes Time)\nN to go back to Segments:  ")
        if val == 'Y':
            output_by_cycle(xml_runs, seg, log)
        else:
            continue

    #print("t0:  %s\nt1:  %s\nt2:  %s" % ((t1 - t0), (t2 - t1), (t3-t4)))